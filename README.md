## Restaurant Service

This is my project for managing the restaurnt, maybe also future business.

Whole WebApp is designed for manage restaurant via web based online system ( phone,tablet,computer with any web browser )

---

## Working project at two different "servers" ( laptops almost always online ): 

[**server #1**]http://czk.myftp.org/Restaurant/

My laptop that i'll try to keep online 24h/7day, just one port if forwarded and open

[**server #2**]http://zielone.myftp.org:8080/Restaurant/

My brother laptop with virtual machine at virtaulbox runing ubuntu server that is mostly 24h/7day online

---

## Owner of the restaurant can : 
	
	TO DO : 
		view orders from day, week, month, year, range of dates and time,
		most popular position in menu
		view all orders even canceled by customer 

## Clinet of the restaurant can : 

	make order from menu
	order option: 
		confirm ( automatically send to kitchen and change status of order )
		cancel ( change status of order to cancel by customer )
		review the order : 
			increase/decrease/remove quantity before confirm order
	order by own account or anonymous account
	select table and eat inside or TO DO order for take out ( add new special table for take outs )

## Waitress of the restaurant can : 

	view orders from customers and react when status of orders change to : 
		- ready to serve
		- close order and pay
		- chose form of pay
 
## Kitchen of the restaurant can :

	accept customer order and change automatically status of order to "in preparing",
	cancel the order ( out of products for prepare )
	change status of order for next one by sort order number in statuses 
	
## Different views for different users: owne, kitchen, waitres, customer.

---

## TO DO:

	when closing the order and paying we can split the bill between the customer at table just by simple view of whole order and main customer 
	add new customer ( adding new column at right side with empty order of the primary customer where is whole order )
	manage orders and order positions : send position with quantity from main order to person/s ( new customers just added ) at left or right side column
		
		MAIN Customer	|	added new customer no.1	|	added new customer no.2
	Position 1 x 3 ->	|
						|	<- position 2 x 1 -> 	|
	position 3 x 1 ->	|	<- position 3 x 1 ->	|	<- postion 3 x 1  
	total 		....	|	total .... 				|	total 	
	
	<- move to customer at left side 
	-> move to customer at righ side 
	
	- security : admin users, customer users, owner,kitchen,waitress and others workers of the restaurant

---

### MORE IN FUTURE 