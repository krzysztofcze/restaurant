package com.czeladko.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="status")
public class Status {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Integer sortNumber; // order from lower number to higher ( to dont have a mess )
	private String name;
	private String description;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the sortNumber
	 */
	public Integer getSortNumber() {
		return sortNumber;
	}
	/**
	 * @param sortNumber the sortNumber to set
	 */
	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @param sortNumber
	 * @param name
	 * @param description
	 */
	public Status(Integer sortNumber, String name, String description) {
		this.sortNumber = sortNumber;
		this.name = name;
		this.description = description;
	}
	/**
	 * 
	 */
	public Status() {
	}
}