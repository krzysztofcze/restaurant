package com.czeladko.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="tableOrder")
public class TableOrder {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	private Table table;
	@OneToOne
	private Status status;
	@OneToMany(fetch=FetchType.EAGER)
	private List<CustomerOrder> customerOrders;
	private String dateStart;
	private String dateEnd;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the table
	 */
	public Table getTable() {
		return table;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	/**
	 * @return the customerOrders
	 */
	public List<CustomerOrder> getCustomerOrders() {
		return customerOrders;
	}
	/**
	 * @param customerOrders the customerOrders to set
	 */
	public void setCustomerOrders(List<CustomerOrder> customerOrders) {
		this.customerOrders = customerOrders;
	}
	/**
	 * @return the dateStart
	 */
	public String getDateStart() {
		return dateStart;
	}
	/**
	 * @param dateStart the dateStart to set
	 */
	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}
	/**
	 * @return the dateEnd
	 */
	public String getDateEnd() {
		return dateEnd;
	}
	/**
	 * @param dateEnd the dateEnd to set
	 */
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}
	/**
	 * @param id
	 * @param table
	 * @param status
	 * @param customerOrders
	 * @param dateStart
	 * @param dateEnd
	 */
	public TableOrder(Long id, Table table, Status status, List<CustomerOrder> customerOrders, String dateStart,
			String dateEnd) {
		this.id = id;
		this.table = table;
		this.status = status;
		this.customerOrders = customerOrders;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
	}
	/**
	 * 
	 */
	public TableOrder() {
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TableOrder [id=" + id + ", table=" + table + ", status=" + status + ", customerOrders=" + customerOrders
				+ ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + "]";
	}
}
