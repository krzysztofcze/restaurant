package com.czeladko.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name="product")
@Transactional
public class Product {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description; 
	@Autowired
	@ManyToOne
	private Category category;
	@Column(nullable=true)
	private double price;
	@Column(nullable=true)
	private boolean available;
	private Integer sortNumber; //sort number in category
	
	private String allergent; // alegenty
	private String photoUrl; // link do zdjecia
	private String videoUrl; // link do zdjecia
	
	/**
	 * @param name
	 * @param description
	 * @param category
	 * @param price
	 * @param available
	 * @param sortNumber
	 */
	public Product(String name, String description,Category category, double price, boolean available,
			Integer sortNumber) {
		this.name = name;
		this.description = description;
//		this.category = category;
		this.price = price;
		this.available = available;
		this.sortNumber = sortNumber;
	}
	/**
	 * 
	 */
	public Product() {
	}
	/**
	 * @param name
	 */
	public Product(String name) {
		this.name = name;
	}
	/**
	 * @param name
	 * @param description
	 * @param category
	 * @param price
	 * @param available
	 * @param sortNumber
	 * @param allergent
	 * @param photoUrl
	 * @param videoUrl
	 */
	public Product(String name, String description, Category category, double price, boolean available,
			Integer sortNumber, String allergent, String photoUrl, String videoUrl) {
		this.name = name;
		this.description = description;
//		this.category = category;
		this.price = price;
		this.available = available;
		this.sortNumber = sortNumber;
		this.allergent = allergent;
		this.photoUrl = photoUrl;
		this.videoUrl = videoUrl;
	}
	/**
	 * @param name
	 * @param description
	 * @param price
	 * @param available
	 * @param sortNumber
	 */
	public Product(String name, String description, double price, boolean available,
			Integer sortNumber) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.available = available;
		this.sortNumber = sortNumber;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the available
	 */
	public boolean isAvailable() {
		return available;
	}
	/**
	 * @param available the available to set
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}
	/**
	 * @return the sortNumber
	 */
	public Integer getSortNumber() {
		return sortNumber;
	}
	/**
	 * @param sortNumber the sortNumber to set
	 */
	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}
	/**
	 * @return the allergent
	 */
	public String getAllergent() {
		return allergent;
	}
	/**
	 * @param allergent the allergent to set
	 */
	public void setAllergent(String allergent) {
		this.allergent = allergent;
	}
	/**
	 * @return the photoUrl
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}
	/**
	 * @param photoUrl the photoUrl to set
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	/**
	 * @return the videoUrl
	 */
	public String getVideoUrl() {
		return videoUrl;
	}
	/**
	 * @param videoUrl the videoUrl to set
	 */
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	
}
