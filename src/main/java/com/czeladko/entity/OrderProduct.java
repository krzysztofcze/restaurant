package com.czeladko.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name="orderProduct")
public class OrderProduct {
	/**
	 * @return the value
	 */
	public Double getValue() {
		setValue(this.product.getPrice()*this.quantity);
		return this.value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne(fetch=FetchType.EAGER)
	private Product product;
	private Integer quantity;
	private Double value;
	
	/**
	 * @param id
	 * @param product
	 * @param quantity
	 */
	public OrderProduct(Long id, Product product, Integer quantity) {
		this.id = id;
		this.product = product;
		this.quantity = quantity;
	}
	/**
	 * @param product
	 * @param quantity
	 */
	public OrderProduct(Product product, Integer quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	/**
	 * 
	 */
	public OrderProduct() {
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}
