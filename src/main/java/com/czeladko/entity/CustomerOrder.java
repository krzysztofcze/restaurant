package com.czeladko.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name = "customerOrder")
public class CustomerOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	private Customer customer;
	@OneToMany(fetch = FetchType.EAGER)
	private List<OrderProduct> orderProduct;
	private String dateStart;
	private String dateEnd;
	@OneToOne
	private Status status;
	private Double total;
	
	/**
	 * @return the total
	 */
	public Double getTotal() {
		double sum = .0 ;
		if (this.orderProduct != null) {
			for (OrderProduct op : this.orderProduct) {
				sum = sum + op.getProduct().getPrice() * op.getQuantity();
			}
		}
		setTotal((double) Math.round(sum*100)/100);
		return this.total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @param id
	 * @param customer
	 * @param orderProduct
	 * @param dateStart
	 * @param dateEnd
	 * @param status
	 */
	public CustomerOrder(Long id, Customer customer, List<OrderProduct> orderProduct, String dateStart, String dateEnd,
			Status status) {
		this.id = id;
		this.customer = customer;
		// this.orderProduct = orderProduct;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.status = status;
	}

	/**
	 * @param customer
	 * @param orderProduct
	 * @param dateStart
	 * @param dateEnd
	 * @param status
	 */
	public CustomerOrder(Customer customer, List<OrderProduct> orderProduct, String dateStart, String dateEnd,
			Status status) {
		this.customer = customer;
		// this.orderProduct = orderProduct;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.status = status;
	}

	/**
	 * 
	 */
	public CustomerOrder() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the orderProduct
	 */
	public List<OrderProduct> getOrderProduct() {
		return orderProduct;
	}

	/**
	 * @param orderProduct
	 *            the orderProduct to set
	 */
	public void setOrderProduct(List<OrderProduct> orderProduct) {
		this.orderProduct = orderProduct;
	}

	/**
	 * @return the dateStart
	 */
	public String getDateStart() {
		return dateStart;
	}

	/**
	 * @param dateStart
	 *            the dateStart to set
	 */
	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * @return the dateEnd
	 */
	public String getDateEnd() {
		return dateEnd;
	}

	/**
	 * @param dateEnd
	 *            the dateEnd to set
	 */
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

}
