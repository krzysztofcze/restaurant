package com.czeladko.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="category")
public class Category {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Integer sortNumber; // order category from lower numbers to higher 
	private String name; // name of category
	private String description;
	private boolean visible;
//	private Category parent; // maybe is sub-category and got parrents
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the sortNumber
	 */
	public Integer getSortNumber() {
		return sortNumber;
	}
	/**
	 * @param sortNumber the sortNumber to set
	 */
	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	/**
	 * 
	 */
	public Category() {
	}
	/**
	 * @param sortNumber
	 * @param name
	 * @param description
	 * @param visible
	 */
	public Category(Integer sortNumber, String name, String description, boolean visible) {
		this.sortNumber = sortNumber;
		this.name = name;
		this.description = description;
		this.visible = visible;
	}

	/**
	 * @param id
	 * @param sortNumber
	 * @param name
	 * @param description
	 * @param visible
	 */
	public Category(Long id, Integer sortNumber, String name, String description, boolean visible) {
		this.id = id;
		this.sortNumber = sortNumber;
		this.name = name;
		this.description = description;
		this.visible = visible;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Category [id=" + id + ", sortNumber=" + sortNumber + ", name=" + name + ", description=" + description
				+ ", visible=" + visible + "]";
	}
}
