package com.czeladko.sitemesh.filter;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/*
======
SOURCE 
======
https://jorosjavajams.wordpress.com/springmvc-javaconfig-and-sitemesh-with-maven/
	
*/

public class MySitemeshFilter extends ConfigurableSiteMeshFilter {

	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		builder
			.addDecoratorPath("/*", "/WEB-INF/views/decorators/decorator.jsp")
			
			.addExcludedPath("/WEB-INF/views/excluded/**")
			.addExcludedPath("/WEB-INF/views/resources/**")
			.addExcludedPath("/WEB-INF/views/decorators/**")
			.addExcludedPath("/WEB-INF/views/frontend/**")
			.addExcludedPath("/frontend/**")
			.addExcludedPath("/swagger-ui**");
		

//		// Map default decorator. This shall be applied to all paths if no other paths
//		// match.
//		builder.addDecoratorPath("/*", "/default-decorator.html")
//				// Map decorators to path patterns.
//				.addDecoratorPath("/admin/*", "/another-decorator.html")
//				.addDecoratorPath("/*.special.jsp", "/special-decorator.html")
//				// Map multiple decorators to the a single path.
//				.addDecoratorPaths("/articles/*", "/decorators/article.html", "/decoratos/two-page-layout.html",
//						"/decorators/common.html")
//				// Exclude path from decoration.
//				.addExcludedPath("/javadoc/*").addExcludedPath("/brochures/*");
	}

}
