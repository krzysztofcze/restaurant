//package com.czeladko.controller;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//import javax.servlet.http.HttpSession;
//import javax.transaction.Transactional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.SessionAttributes;
//
//import com.czeladko.entity.Category;
//import com.czeladko.entity.Customer;
//import com.czeladko.entity.CustomerOrder;
//import com.czeladko.entity.OrderProduct;
//import com.czeladko.entity.Product;
//import com.czeladko.entity.Table;
//import com.czeladko.repository.CategoryRepository;
//import com.czeladko.repository.CustomerOrderRepository;
//import com.czeladko.repository.CustomerRepository;
//import com.czeladko.repository.OrderProductRepository;
//import com.czeladko.repository.ProductRepository;
//import com.czeladko.repository.TableRepository;
//
//@Controller
//@RequestMapping("menu/")
//@SessionAttributes({ "customerTable", "currentCustomer", "currentCustomerOrder" })
//@Transactional
//public class MenuController {
//
//	@Autowired
//	private ProductRepository productRepository;
//
//	@Autowired
//	private CategoryRepository categoryRepository;
//
//	@Autowired
//	private CustomerRepository customerRepository;
//
//	@Autowired
//	private CustomerOrderRepository customerOrderRepository;
//
//	@Autowired
//	private OrderProductRepository orderProductRepository;
//
//	@Autowired
//	private TableRepository tableRepository;
//
//	private Collection<Category> categoryFindAllMine() {
//		return categoryRepository.findAllByOrderBySortNumberAscNameAsc();
//	}
//
//	private Collection<Category> categoryFindAllMine(boolean visible) {
//		return categoryRepository.findAllByVisibleOrderBySortNumberAscNameAsc(visible);
//	}
//
//	private Collection<Product> productFindAllMineByCatAndAvailable(Category category, boolean available) {
//		return productRepository.findAllByCategoryAndAvailableOrderBySortNumberAscNameAsc(category, available);
//	}
//
//	@GetMapping("")
//	public String home(Model model, HttpSession session,
//			@RequestParam(required = false, name = "available", defaultValue = "true") boolean available) {
//
//		LinkedHashMap<Category, Collection<Product>> menu = new LinkedHashMap<>();
//
//		if (available == false) {
//			Collection<Category> categories = categoryFindAllMine();
//			Collection<Product> listaProduktow;
//			for (Category cat : categories) {
//				listaProduktow = productRepository.findAllByCategoryOrderBySortNumberAscNameAsc(cat);
//				menu.put(cat, listaProduktow);
//
//			}
//		} else if (available == true) {
//			Collection<Category> categories = categoryFindAllMine(available);
//			Collection<Product> listaProduktow;
//			for (Category cat : categories) {
//				listaProduktow = productFindAllMineByCatAndAvailable(cat, available);
//				menu.put(cat, listaProduktow);
//			}
//		}
//		model.addAttribute("menu", menu);
//
//		Customer customer = new Customer();
//		model.addAttribute("customer", customer);
//
//		Collection<Customer> customers = customerRepository.findAll();
//		model.addAttribute("customers", customers);
//
//		Collection<CustomerOrder> collectionCustomerOrder = customerOrderRepository.findAll();
//		model.addAttribute(collectionCustomerOrder);
//
//		Table table = new Table();
//		model.addAttribute("table", table);
//		Collection<Table> collectionTable = tableRepository.findAll();
//		model.addAttribute("tables", collectionTable);
//
//		return "pages/menu";
//	}
//
//	@GetMapping("/order")
//	public String addToOrder(HttpSession session, @RequestParam(name = "id") Long id, Model model) {
//
//		Customer currentCustomer = new Customer();
//		if (session.getAttribute("currentCustomer") != null) {
//			currentCustomer = (Customer) session.getAttribute("currentCustomer");
//		}
//		Table customerTable = new Table();
//		if (session.getAttribute("customerTable") != null) {
//			customerTable = (Table) session.getAttribute("customerTable");
//		}
//		CustomerOrder currentCustomerOrder = new CustomerOrder();
//		if (session.getAttribute("currentCustomerOrder") != null) {
//			currentCustomerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
//		}
//		Product nowyProduct = new Product();
//		nowyProduct = productRepository.findOne(id);
//
//		List<OrderProduct> zamowienie = new ArrayList<OrderProduct>();
//		if (currentCustomerOrder.getOrderProduct() != null) {
//			boolean czyDopisaneZostalo = false;
//			zamowienie = currentCustomerOrder.getOrderProduct();
//			for (OrderProduct op : zamowienie) {
//				if (op.getProduct().getId() == nowyProduct.getId()) {
//					Integer temp = op.getQuantity();
//					op.setQuantity(++temp);
//					orderProductRepository.save(op);
//					czyDopisaneZostalo = true;
//				}
//			}
//			if (!czyDopisaneZostalo) {
//				OrderProduct newop = new OrderProduct(nowyProduct, 1);
//				orderProductRepository.save(newop);
//				zamowienie.add(newop);
//			}
//
//		} else {
//			OrderProduct newop = new OrderProduct(nowyProduct, 1);
//			orderProductRepository.save(newop);
//			zamowienie.add(newop);
//		}
//		
//		currentCustomerOrder.setCustomer(currentCustomer);
//		currentCustomerOrder.setOrderProduct(zamowienie);
//		customerOrderRepository.save(currentCustomerOrder);
//		model.addAttribute("currentCustomerOrder", currentCustomerOrder);
//
//		// tableRepository.save(customerTable);
//		// model.addAttribute("customerTable", customerTable);
//		//
//		//
//		// customerRepository.save(currentCustomer);
//		// model.addAttribute("currentCustomer", currentCustomer);
//
//		return "redirect:/menu/";
//	}
//
//	@ModelAttribute(name = "categoriesSorted")
//	public Collection<Category> listCategories() {
//		Collection<Category> categories = categoryFindAllMine();
//		return categories;
//	}
//
//	/*
//	 * @PostMapping("/setCustomer") public ModelAndView setCustomer(HttpSession
//	 * session, @RequestParam(name = "id") Long id, Model model, HttpServletResponse
//	 * response) { String stringId = String.valueOf(id); Cookie myCookie = new
//	 * Cookie("currentCustomerId", stringId); myCookie.setPath("/");
//	 * response.addCookie(myCookie);
//	 * 
//	 * Customer customer = new Customer(); customer =
//	 * customerRepository.findOne(id); model.addAttribute("currentCustomer",
//	 * customer); customer = customerRepository.findOne(id);
//	 * session.setAttribute("currentCustomer", customer);
//	 * 
//	 * String redirect = "redirect:/menu/"; ModelAndView mav = new ModelAndView();
//	 * mav.addObject("customer", customer); mav.setViewName(redirect); return mav; }
//	 */
//
//	/*
//	 * @PostMapping("/setTable") public ModelAndView setTable(HttpSession
//	 * session, @RequestParam(name = "id") Long id, Model model, HttpServletResponse
//	 * response) { String stringId = String.valueOf(id); Cookie myCookie = new
//	 * Cookie("currentCustomerTableId", stringId); myCookie.setPath("/");
//	 * response.addCookie(myCookie);
//	 * 
//	 * Table table = new Table(); table = tableRepository.findOne(id);
//	 * model.addAttribute("currentCustomerTableId", table);
//	 * 
//	 * session.setAttribute("currentCustomerTableId", table);
//	 * 
//	 * String redirect = "redirect:/menu/"; ModelAndView mav = new ModelAndView();
//	 * mav.addObject("table", table); mav.setViewName(redirect); return mav; }
//	 */
//
//}
