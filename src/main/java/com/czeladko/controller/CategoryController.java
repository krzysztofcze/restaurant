package com.czeladko.controller;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Category;
import com.czeladko.repository.CategoryRepository;

@Controller
@RequestMapping({ "/categories/", "/category/" })
@Transactional
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	private Collection<Category> findAllMine() {
		return categoryRepository.findAllByOrderBySortNumberAscNameAsc();
	}

	private Collection<Category> findAllMine(boolean visible) {
		return categoryRepository.findAllByVisibleOrderBySortNumberAscNameAsc(visible);
	}
	
	@GetMapping("")
	public String showAll(Model model, @RequestParam(name = "edit", required = false) Long id) {
		if (id != null) {
			if (id == 0) {
				Category category = new Category();
				model.addAttribute("category", category);
			} else {
				Category category = new Category();
				category = categoryRepository.findOne(id);
				model.addAttribute("category", category);
			}
		}
		Collection<Category> categories = findAllMine();
		model.addAttribute("categories", categories);
		return "pages/category";
	}
	
	@PostMapping("")
	public ModelAndView addOrEdit(@ModelAttribute Category category) {
		categoryRepository.save(category);
		return new ModelAndView("redirect:/category/");
	}

	@GetMapping("move")
	public ModelAndView move(Model model, @RequestParam(name = "move", required = false) Integer move,
			@RequestParam(name = "id", required = false) Long id) {
		Category category = categoryRepository.findOne(id);
		if (category.getSortNumber() == null)
			category.setSortNumber(0);
		category.setSortNumber(category.getSortNumber() + move);
		categoryRepository.save(category);
		Collection<Category> categories = findAllMine();
		model.addAttribute("categories", categories);
		return new ModelAndView("redirect:/categories/");
	}

	@GetMapping("delete")
	public ModelAndView move(Model model, @RequestParam(name = "id", required = false) Long id) {
		if (id != null && categoryRepository.exists(id)) {
			try {
				categoryRepository.delete(id);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		Collection<Category> categories = findAllMine();
		model.addAttribute("categories", categories);
		return new ModelAndView("redirect:/categories/");
	}

}
