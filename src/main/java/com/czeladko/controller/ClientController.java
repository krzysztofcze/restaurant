package com.czeladko.controller;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Customer;
import com.czeladko.repository.CustomerRepository;
import com.czeladko.services.CustomerService;

@Controller
@Transactional
@RequestMapping({ "/client/" })
@SessionAttributes({"currentCustomer","msgAfterPost","Customer","client"})
public class ClientController {
	
	private static final String PAGES_CUSTOMER_LOGIN = "pages/customer_login";
	private static final String REDIRECT_CLIENT = "redirect:/client/";
	
	@Autowired
	Validator validator;
	
	@Autowired
	private CustomerService customerSerice;

	@Autowired
	private CustomerRepository customerRepository;
	
	@ModelAttribute(name="Customer")
	public Customer getModelCustomer() {
		return new Customer();
	}
	
	@ModelAttribute(name="client")
	public Customer getModelClient() {
		return new Customer();
	}
	
	@GetMapping("")
	public ModelAndView getMapping() {
		return new ModelAndView(PAGES_CUSTOMER_LOGIN);
	}

	@GetMapping(path="",params="logout=true")
	public String Logout(SessionStatus sessionStatus,Model model) {
		sessionStatus.setComplete();
		String msgAfterPost = "SUCCESS: You are loged out." ;
		model.addAttribute("msgAfterPost",msgAfterPost);
		sessionStatus.setComplete();
		return REDIRECT_CLIENT;
	}
	
	@PostMapping(path="",params="formName=loginAsAnonymous")
	public String loginAsAnonymous(Model model) {
		Customer currentCustomer = customerSerice.createNewAnonymouseCustomer();
		model.addAttribute("currentCustomer", currentCustomer);
		String msgAfterPost = "SUCCESS: we created new anyonymous account for You." ;
		model.addAttribute("msgAfterPost",msgAfterPost);
		return PAGES_CUSTOMER_LOGIN;
	}

	@PostMapping(path="",params="formName=login")
	public String login(Model model, @RequestParam(required = false) String email,
			@RequestParam(required = false) String password) {
		if (email != null && password != null) {
			if (customerSerice.login(email, password)) {
				Customer currentCustomer = customerRepository.findCustomerByEmail(email);
				model.addAttribute("currentCustomer", currentCustomer);
				String msgAfterPost = "SUCCESS: Login and password match." ;
				model.addAttribute("msgAfterPost",msgAfterPost);
				return PAGES_CUSTOMER_LOGIN;
			}
		}
		String msgAfterPost = "FAIL : login and passord dont match or exist." ;
		model.addAttribute("msgAfterPost",msgAfterPost);
		return PAGES_CUSTOMER_LOGIN;

	}
	
	@PostMapping(path="",params="formName=register")
	public String login(@ModelAttribute("client") @Valid Customer customer,BindingResult result,Model model) {
		if(result.hasErrors()) {
			String msgAfterPost = "FAILED to register." ;
			model.addAttribute("msgAfterPost",msgAfterPost);
			return PAGES_CUSTOMER_LOGIN;
		}
		if(customerRepository.findCustomerByEmail(customer.getEmail())!=null) {
			String msgAfterPost = "FAILED to register, email already in use." ;
			model.addAttribute("msgAfterPost",msgAfterPost);
			return PAGES_CUSTOMER_LOGIN;
		} 
		String msgAfterPost = "SUCCESS : customer registered and created." ;
		model.addAttribute("msgAfterPost",msgAfterPost);
		customerRepository.saveAndFlush(customer);
		return PAGES_CUSTOMER_LOGIN;
	}
	
}
