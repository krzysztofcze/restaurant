package com.czeladko.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.czeladko.entity.Customer;
import com.czeladko.entity.Table;
import com.czeladko.repository.CustomerRepository;
import com.czeladko.repository.TableRepository;

@Controller
@RequestMapping("/cookiesandsession/")
@Transactional
public class CookiesAndSessionController {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private TableRepository tableRepository;

	@GetMapping("/")
	public String showAll(HttpSession session, Model model, HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		List<String> ciasteczka = new ArrayList<String>();
		StringBuffer listac = new StringBuffer();
		if (cookies != null) {
			for (Cookie c : cookies) {
				ciasteczka.add(c.getName() + " " + c.getValue() + "\n <br>");
				listac.append(c.getComment() + " - getComment\n <br>").append(c.getDomain() + " - getDomain\n <br>")
						.append(c.getMaxAge() + " maxAge\n <br> ").append(c.getName() + " -name\n <br>")
						.append(c.getPath() + " - path\n <br>").append(c.getSecure() + " - secure\n <br>")
						.append(c.getValue() + " - value\n <br>").append(c.getVersion() + " - version\n <br>")
						.append(c.getClass() + " - class.\n <br>");
			}
		}
		model.addAttribute("ciasteczka", ciasteczka);
		model.addAttribute("listac", listac);

		Enumeration<String> sesja = session.getAttributeNames();
		System.out.println(sesja.toString());
		List<String> listas = new ArrayList<String>();
		while (sesja.hasMoreElements()) {
			String param = sesja.nextElement();
			listas.add(param);
		}
		Customer customer = (Customer) session.getAttribute("currentCustomer");
		Table table = (Table) session.getAttribute("currentCustomerTableId");

		model.addAttribute("sesja", sesja);
		model.addAttribute("listas", listas);

		String s = "";

		s = s + " " + request.isRequestedSessionIdFromCookie() + " request.isRequestedSessionIdFromCookie()\n<br>";
		s = s + " " + request.isRequestedSessionIdFromURL() + " request.isRequestedSessionIdFromURL()\n<br>";
		s = s + " " + request.isRequestedSessionIdValid() + " request.isRequestedSessionIdValid()\n<br>";
		model.addAttribute("s", s);
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			model
				.addAttribute("uri", request.getRequestURI())
				.addAttribute("user", auth.getName())
				.addAttribute("roles", auth.getAuthorities());
		
		return "pages/cookiesandsession";
	}
}
