package com.czeladko.controller;

import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.czeladko.entity.Customer;
import com.czeladko.entity.Table;
import com.czeladko.repository.CustomerRepository;
import com.czeladko.repository.TableRepository;
import com.czeladko.services.CustomerService;

@Controller
@Transactional
@RequestMapping("/")
@SessionAttributes({ "customerTable", "currentCustomer", "currentCustomerOrder" ,"customer"})
public class HomeController {

	private static final String REDIRECT = "redirect:/";

	private static final String PAGES_HOME = "pages/home";

	@Autowired
	private CustomerService customerService;
	@Autowired
	private TableRepository tableRepository;
	@Autowired
	private CustomerRepository customerRepository;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@GetMapping("/")
	public String home(Model model, HttpSession session) {

		Collection<Table> tables = tableRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("tables", tables);

		Customer customer = (Customer) session.getAttribute("currentCustomer");
		if (customer != null) {
			if (customer.getId() != null) {
				if (customerRepository.exists(customer.getId())) {
					return PAGES_HOME;
				}
			}
		} else if (customer == null) {
			customer = customerService.createNewAnonymouseCustomer();
		}
		model.addAttribute("currentCustomer", customer);
		return PAGES_HOME;
	}

	@GetMapping("anonymous")
	public String anonymous(Model model, HttpSession session) {
		Customer customer = customerService.createNewAnonymouseCustomer();
		model.addAttribute("currentCustomer", customer);
		return PAGES_HOME;
	}

	@GetMapping("customer_logout")
	public String logout(Model model) {
		Customer customer = new Customer();
		model.addAttribute("currentCustomer", customer);
		return REDIRECT;
	}

	@GetMapping({ "customer_login", "customer_register" })
	public String login(Model model) {
		return REDIRECT;
	}

	@PostMapping("/")
	public String set(HttpSession session, Model model,
			@RequestParam(name = "setTable", required = false) Long tableId) {
		if ((tableId != null)) {
			if (tableRepository.exists(tableId)) {
				Table table = tableRepository.findOne(tableId);
				model.addAttribute("customerTable", table);
			}
		}
		return PAGES_HOME;
	}

	@PostMapping("/customer_login")
	public String login(HttpSession session, Model model, @Valid Customer customer, BindingResult result,
			@RequestParam(name = "email") String email, @RequestParam(name = "password") String password) {
		if (result.hasErrors()) {
			return PAGES_HOME;
		} else {
			boolean checkPass = false;
			checkPass = customerService.login(email, password);
			if (checkPass) {
				Customer customer2 = customerRepository.findCustomerByEmail(email);
				model.addAttribute("currentCustomer", customer2);
				session.setAttribute("currentCustomer", customer2);
			}
		}
		return PAGES_HOME;
	}

	@GetMapping("/menu")
	public String menu(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		return PAGES_HOME;
	}

	@PostMapping("/customer_register")
	public String register(@Valid Customer customer, BindingResult result) {
		if (result.hasErrors()) {
			return PAGES_HOME;
		} else {
			if (!customerService.customerExistWithEmail(customer.getEmail())) {
				customerRepository.save(customer);
			}
			return PAGES_HOME;
		}
	}
	
	@ModelAttribute(name = "Customer")
	public Customer returncustomer() {
		return new Customer();
	}

}
