package com.czeladko.controller;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Table;
import com.czeladko.repository.TableRepository;

@Controller
@RequestMapping({ "/tables/", "/table/" })
@Transactional
public class TableController {

	@Autowired
	private TableRepository tableRepository;

	@GetMapping("")
	public String showAll(Model model, @RequestParam(name = "edit", required = false) Long id,
			@RequestParam(name = "sort", required = false) String sort) {
		if (id != null) {
			if (id == 0) {
				Table table = new Table();
				model.addAttribute("table", table);
			} else {
				Table table = new Table();
				table = tableRepository.findOne(id);
				model.addAttribute("table", table);
			}
		}
		Collection<Table> tables = new ArrayList<Table>();
		if (sort != null) {
			if (sort.equalsIgnoreCase("desc")) {
				tables = tableRepository.findAllByOrderBySortNumberDesc();
			} else {
				tables = tableRepository.findAllByOrderBySortNumberAsc();
			}
		} else {
			tables = tableRepository.findAllByOrderBySortNumberAsc();
		}
		model.addAttribute("tables", tables);
		return "pages/table";
	}

	@GetMapping("move")
	public ModelAndView move(Model model, @RequestParam(name = "move", required = false) Integer move,
			@RequestParam(name = "id", required = false) Long id) {
		Table table = tableRepository.findOne(id);
		table.setSortNumber(table.getSortNumber() + move);
		tableRepository.save(table);
		Collection<Table> tables = tableRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("tables", tables);
		return new ModelAndView("redirect:/tables/");
	}

	@GetMapping("delete")
	public ModelAndView move(Model model, @RequestParam(name = "id", required = false) Long id) {
		tableRepository.delete(id);
		Collection<Table> tables = tableRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("tables", tables);
		return new ModelAndView("redirect:/tables/");
	}

	@PostMapping("")
	public ModelAndView addOrEdit(@ModelAttribute Table table) {
		tableRepository.save(table);
		return new ModelAndView("redirect:/tables/");
	}

}
