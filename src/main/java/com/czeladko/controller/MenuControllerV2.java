package com.czeladko.controller;

import java.util.Collection;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.czeladko.entity.Category;
import com.czeladko.entity.Customer;
import com.czeladko.entity.CustomerOrder;
import com.czeladko.entity.Product;
import com.czeladko.entity.Table;
import com.czeladko.repository.CategoryRepository;
import com.czeladko.repository.CustomerOrderRepository;
import com.czeladko.repository.CustomerRepository;
import com.czeladko.repository.TableRepository;
import com.czeladko.services.CustomerOrderService;
import com.czeladko.services.MenuService;

@Controller
@RequestMapping("menu/")
@SessionAttributes({ "customerTable", "currentCustomer", "currentCustomerOrder" })
@Transactional
public class MenuControllerV2 {

	private static final String REDIRECT_MENU = "redirect:/menu/";

	private static final String PAGES_MENU = "pages/menu";

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerOrderRepository customerOrderRepository;

	@Autowired
	private TableRepository tableRepository;

	@Autowired
	private CustomerOrderService customerOrderService;

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("")
	public String home(Model model, HttpSession session,
			@RequestParam(required = false, name = "available", defaultValue = "true") boolean available) {

		LinkedHashMap<Category, Collection<Product>> menu = menuService.genarate_menu(available);
		model.addAttribute("menu", menu);

		Customer customer = new Customer();
		model.addAttribute("customer", customer);

		Collection<Customer> customers = customerRepository.findAll();
		model.addAttribute("customers", customers);

		Collection<CustomerOrder> collectionCustomerOrder = customerOrderRepository.findAll();
		model.addAttribute(collectionCustomerOrder);

		Table table = new Table();
		model.addAttribute("table", table);
		
		Collection<Table> collectionTable = tableRepository.findAll();
		model.addAttribute("tables", collectionTable);
		
		Collection<Category> categories = categoryRepository.findAllByOrderBySortNumberAscNameAsc();
		model.addAttribute("categories",categories);
		return PAGES_MENU;
	}

	@GetMapping("/order")
	public String addToOrder(HttpSession session, @RequestParam(name = "id") Long id, Model model) {
		
		CustomerOrder currentCustomerOrder = customerOrderService.newEmpty();
		if (session.getAttribute("currentCustomerOrder") != null) {
			currentCustomerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
		}
		currentCustomerOrder = customerOrderService.productAdd(currentCustomerOrder, id);
		model.addAttribute("currentCustomerOrder", currentCustomerOrder);
		return REDIRECT_MENU;
	
	}

	@ModelAttribute(name = "categoriesSorted")
	public Collection<Category> listCategories() {
		return menuService.getCategoriesSorted();
	}

}
