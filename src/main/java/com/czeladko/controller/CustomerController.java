package com.czeladko.controller;

import java.util.Collection;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Customer;
import com.czeladko.repository.CustomerRepository;

@Controller
@RequestMapping({ "/customers/", "/customer/" })
@Transactional
public class CustomerController {

	private static final String REDIRECT_CUSTOMERS = "redirect:/customers/";

	private static final String PAGES_CUSTOMER = "pages/customer";

	@Autowired
	Validator validator;

	@Autowired
	private CustomerRepository customerRepository;

	// get ALL + get ADD AND get EDIT
	@GetMapping("/")
	public String showAll(Model model, @RequestParam(name = "edit", required = false) Long id,
			@RequestParam(name = "add", required = false) Long add) {
		
		if (id != null) {
			Customer customer = new Customer();
			customer = customerRepository.findOne(id);
			model.addAttribute("customer", customer);
		}
		
		if (add != null) {
			Customer customer = new Customer();
			model.addAttribute("customer", customer);
		}
		
		Collection<Customer> customers = customerRepository.findAll();
		model.addAttribute("customers", customers);
		return PAGES_CUSTOMER;
	}

	// POST ADD or EDIT
	@PostMapping("/")
	public ModelAndView addOrEdit(@Valid Customer customer, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView MAV = new ModelAndView();
			String address="";
			if (customer.getId() == null) {
				customer.setId(0L);
				address = "redirect:/customers/?add=1";
			} else {
				address = "redirect:/customers/?edit="+customer.getId();
			}
			Collection<Customer> customers = customerRepository.findAll();
			MAV.addObject("customer", customer);
			MAV.addObject("customers", customers);
			address=PAGES_CUSTOMER;
			MAV.setViewName(address);
			return MAV;
		}
		customerRepository.save(customer);
		return new ModelAndView(REDIRECT_CUSTOMERS);
	}

	// DELETE
	@GetMapping("/delete")
	public ModelAndView move(Model model, @RequestParam(name = "id", required = false) Long id) {
		if (customerRepository.exists(id))
			customerRepository.delete(id);
//		Collection<Customer> customers = customerRepository.findAll();
//		model.addAttribute("customers", customers);
		return new ModelAndView(REDIRECT_CUSTOMERS);
	}

}
