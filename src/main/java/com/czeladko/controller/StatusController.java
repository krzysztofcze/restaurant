package com.czeladko.controller;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Status;
import com.czeladko.repository.StatusRepository;

@Controller
@RequestMapping("/statuses/")
@Transactional
public class StatusController {

	@Autowired
	private StatusRepository statusRepository;

	@GetMapping("")
	public String showAll(Model model, @RequestParam(name = "edit", required = false) Long id) {
		if (id != null) {
			if (id == 0) {
				Status status = new Status();
				model.addAttribute("status", status);
			} else {
				Status status = new Status();
				status = statusRepository.findOne(id);
				model.addAttribute("status", status);
			}
		}
		Collection<Status> statuses = statusRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("statuses", statuses);
		// return "status/index";
		return "pages/status";
	}

	@GetMapping("move")
	public ModelAndView move(Model model, @RequestParam(name = "move", required = false) Integer move,
			@RequestParam(name = "id", required = false) Long id) {
		Status status = statusRepository.findOne(id);
		if (status.getSortNumber() == null)
			status.setSortNumber(0);
		status.setSortNumber(status.getSortNumber() + move);
		statusRepository.save(status);
		Collection<Status> statuses = statusRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("statuses", statuses);
		return new ModelAndView("redirect:/statuses/");
	}

	@GetMapping("delete")
	public ModelAndView move(Model model, @RequestParam(name = "id", required = false) Long id) {
		statusRepository.delete(id);
		Collection<Status> statuses = statusRepository.findAllByOrderBySortNumberAsc();
		model.addAttribute("statuses", statuses);
		return new ModelAndView("redirect:/statuses/");
	}

	@PostMapping("")
	public ModelAndView addOrEdit(@ModelAttribute Status status) {
		statusRepository.save(status);
		return new ModelAndView("redirect:/statuses/");
	}

}
