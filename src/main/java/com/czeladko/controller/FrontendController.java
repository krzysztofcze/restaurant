package com.czeladko.controller;

import javax.transaction.Transactional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Transactional
public class FrontendController {

	@GetMapping("/frontend/")
	public String frontend() {
		return "frontend/index";
	}
}
