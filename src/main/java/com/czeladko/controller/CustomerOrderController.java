package com.czeladko.controller;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.czeladko.entity.CustomerOrder;
import com.czeladko.repository.CustomerOrderRepository;
import com.czeladko.services.CustomerOrderService;

@Controller
@RequestMapping({ "/customersorder/", "/customerorder/" })
@SessionAttributes({ "customerTable", "currentCustomer", "currentCustomerOrder" })
@Transactional
public class CustomerOrderController {

	@Autowired
	private CustomerOrderService customerOrderService;

	@Autowired
	private CustomerOrderRepository customerOrderRepository;

	@GetMapping("/")
	public String get(HttpSession session, Model model) {
		CustomerOrder currentCustomerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
		if (currentCustomerOrder != null) {
			Long id = currentCustomerOrder.getId();
			if (id != null) {
				currentCustomerOrder = customerOrderRepository.findOne(id);
			}
			model.addAttribute("currentCustomerOrder", currentCustomerOrder);
		}
		return "pages/customerorder";
	}

	@PostMapping("/")
	public String accept(HttpSession session, @RequestParam(name = "accept", required = false) String accept,
			@RequestParam(name = "cancel", required = false) String cancel, Model model) {
		CustomerOrder currentCustomerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
		if (accept != null) {
			currentCustomerOrder = customerOrderService.accept(currentCustomerOrder);
		}
		if (cancel != null) {
			currentCustomerOrder = customerOrderService.cancelByCustomer(currentCustomerOrder);
		}
		model.addAttribute("currentCustomerOrder", currentCustomerOrder);
		return "redirect:/customerorder/";
	}

	@GetMapping("/delete")
	public String delete(HttpSession session, @RequestParam Long id) {
		CustomerOrder customerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
		customerOrderService.productRemove(customerOrder, id);
		return "redirect:/customerorder/";
	}

	@GetMapping("/quantity")
	public String quantity(HttpSession session, @RequestParam Long id, @RequestParam Integer change) {
		CustomerOrder customerOrder = (CustomerOrder) session.getAttribute("currentCustomerOrder");
		customerOrder = customerOrderService.updateQuantity(customerOrder, id, change);
		return "redirect:/customerorder/";
	}
}
