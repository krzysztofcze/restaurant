package com.czeladko.controller.rest;

import java.io.IOException;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.Category;
import com.czeladko.repository.CategoryRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping({ "/category" })
@Transactional
public class CategoryControllerREST {	

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@GetMapping("")
	public Collection<Category> findAll(@RequestParam(required = false) String sort) {
		Collection<Category> categories = null;
		if (sort != null) {
			switch (sort.toLowerCase()) {
			case "asc":
				categories = categoryRepository.findAllByOrderBySortNumberAsc();
				break;

			case "desc":
				categories = categoryRepository.findAllByOrderBySortNumberDesc();
				break;

			default:
				categories = categoryRepository.findAll();
			}
		} else {
			categories = categoryRepository.findAll();
		}
		return categories;
	}

	@GetMapping("/{id}")
	public Category findOne(@PathVariable Long id) {
		return categoryRepository.findOne(id);
	}

	@PostMapping("")
	public void postOne(@RequestBody Category category) {
		categoryRepository.saveAndFlush(category);
	}

	@PutMapping("/{id}")
	public void putOne(@PathVariable Long id, @RequestBody Category category) {
		category.setId(id);
		categoryRepository.saveAndFlush(category);
	}

	@PatchMapping("/{id}")
	public void patchPart(@PathVariable Long id, @RequestBody Category updateCategory)
			throws JsonProcessingException, IOException {
		Category category = categoryRepository.findOne(id);
		String updateCategoryString = objectMapper.writeValueAsString(updateCategory);
		category = objectMapper.readerForUpdating(category).readValue(updateCategoryString);
		categoryRepository.save(category);
	}

	@DeleteMapping("/{id}")
	public void deleteOne(@PathVariable Long id) {
		categoryRepository.delete(id);
	}

}
