package com.czeladko.controller.rest;

import java.io.IOException;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.Product;
import com.czeladko.repository.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping({ "/product" })
@Transactional
public class ProductControllerREST {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private ProductRepository productRepository;

	@GetMapping("")
	public Collection<Product> findAll(@RequestParam(required = false) String sort) {
		Collection<Product> products = null;
		if (sort != null) {
			switch (sort.toLowerCase()) {
			case "asc":
				products = productRepository.findAllByOrderBySortNumberAscNameAsc();
				break;

			case "desc":
				products = productRepository.findAllByOrderBySortNumberAscNameDesc();
				break;

			default:
				products = productRepository.findAll();
			}
		} else {
			products = productRepository.findAll();
		}
		return products;
	}

	@GetMapping("/{id}")
	public Product findOne(@PathVariable Long id) {
		return productRepository.findOne(id);
	}
	
	@PostMapping("")
	public void postOne(@RequestBody Product producs) {
		productRepository.saveAndFlush(producs);
	}
	
	@PutMapping("/{id}")
	public void putOne(@PathVariable Long id,@RequestBody Product product) {
		product.setId(id);
		productRepository.saveAndFlush(product);
	}
	
	@PatchMapping("/{id}")
	public void patchPart(@PathVariable Long id,@RequestBody Product updateProduct) throws JsonProcessingException, IOException {
		Product product = productRepository.findOne(id);
		String updateProductString = objectMapper.writeValueAsString(updateProduct);
		product = objectMapper.readerForUpdating(product).readValue(updateProductString);
		productRepository.save(product);
	}
	
	@DeleteMapping("/{id}")
	public void deleteOne(@PathVariable Long id) {
		productRepository.delete(id);
	}

}
