package com.czeladko.controller.rest;

import java.util.Collection;
import java.util.LinkedList;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.CustomerOrder;
import com.czeladko.entity.OrderProduct;
import com.czeladko.repository.CustomerOrderRepository;
import com.czeladko.services.CustomerOrderService;

@RestController
@RequestMapping("/order")
@Transactional
public class OrderControllerREST {

	@Autowired
	private CustomerOrderRepository customerOrderRepository;

	@Autowired
	private CustomerOrderService customerOrderService;

	@GetMapping("")
	public Collection<?> get() {
		Collection<CustomerOrder> customerOrders = customerOrderRepository.findAll();
		return customerOrders;
	}

	@GetMapping("/{id}")
	public CustomerOrder viewClientOrder(@PathVariable Long id) {
		return customerOrderRepository.findOne(id);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		customerOrderRepository.delete(id);
	}

	@GetMapping("/{id}/")
	public Collection<?> viewClientProductOrder(@PathVariable Long id) {
		return customerOrderRepository.findOne(id).getOrderProduct();
	}

	@GetMapping("/{id}/{positionFromOrder}")
	public OrderProduct viewClientProductOrderPosition(@PathVariable Long id, @PathVariable Integer positionFromOrder) {
		return customerOrderRepository.findOne(id).getOrderProduct().get(--positionFromOrder);
	}

	@DeleteMapping("/{id}/{positionFromOrder}")
	public void deleteClientProductOrderPosition(@PathVariable Long id, @PathVariable int positionFromOrder) {
		CustomerOrder co = customerOrderRepository.findOne(id);
		co.getOrderProduct().remove(--positionFromOrder);
		customerOrderRepository.saveAndFlush(co);
	}

	@GetMapping("/{id}/{positionFromOrder}/quantity/++")
	public CustomerOrder increaseQuantity(@PathVariable Long id, @PathVariable Integer positionFromOrder) {
		Integer po = positionFromOrder - 1;
		CustomerOrder co = customerOrderRepository.findOne(id);
		Integer quantity = co.getOrderProduct().get(po).getQuantity();
		if (quantity > 0)
			co.getOrderProduct().get(po).setQuantity(++quantity);
		customerOrderRepository.saveAndFlush(co);
		return co;
	}

	@GetMapping("/{id}/{positionFromOrder}/quantity/--")
	public CustomerOrder decreaseQuantity(@PathVariable Long id, @PathVariable Integer positionFromOrder) {
		Integer po = positionFromOrder - 1;
		CustomerOrder co = customerOrderRepository.findOne(id);
		Integer quantity = co.getOrderProduct().get(po).getQuantity();
		if (quantity > 0)
			co.getOrderProduct().get(po).setQuantity(--quantity);
		customerOrderRepository.saveAndFlush(co);
		return co;
	}

	@GetMapping("/kitchen")
	public LinkedList<CustomerOrder> viewForKitchen() {
		return customerOrderService.viewForKitchen();
	}

	@GetMapping("/waitress")
	public LinkedList<CustomerOrder> viewForWaitres() {
		return customerOrderService.viewForWaitress();
	}

	@GetMapping("/{id}/cancelByKitchen")
	public CustomerOrder cancelOrderByKitchen(@PathVariable Long id) {
		CustomerOrder currentCustomerOrder = customerOrderRepository.findOne(id);
		customerOrderService.cancelByKitchen(currentCustomerOrder);
		return currentCustomerOrder;
	}
	
	@GetMapping("/{id}/cancelByCustomer")
	public CustomerOrder cancelOrderByCustomer(@PathVariable Long id) {
		CustomerOrder currentCustomerOrder = customerOrderRepository.findOne(id);
		customerOrderService.cancelByCustomer(currentCustomerOrder);
		return currentCustomerOrder;
	}

	@GetMapping("/{id}/next")
	public CustomerOrder next(@PathVariable Long id) {
		CustomerOrder currentCustomerOrder = customerOrderRepository.findOne(id);
		customerOrderService.next(currentCustomerOrder);
		return currentCustomerOrder;
	}

	@PostMapping("")
	public void post(@Valid CustomerOrder customerOrder) {

	}
	
}