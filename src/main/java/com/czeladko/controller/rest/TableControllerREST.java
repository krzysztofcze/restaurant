package com.czeladko.controller.rest;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.Table;
import com.czeladko.repository.TableRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/table")
@Transactional
public class TableControllerREST {

	@Autowired
	private TableRepository entityRepository;

	@Autowired
	private ObjectMapper objectMapper;
	
	@GetMapping("")
	/*public Collection<?> findAll(@RequestParam(required = false) String sort) {*/
	public Collection<?> read(@RequestParam(required = false) String sort) {
		Collection<?> entity = null;
		if (sort != null) {
			switch (sort.toLowerCase()) {
			case "asc":
				entity = entityRepository.findAllByOrderBySortNumberAsc();
				break;

			case "desc":
				entity = entityRepository.findAllByOrderBySortNumberDesc();
				break;

			default:
				entity = entityRepository.findAll();
			}
		} else {
			entity = entityRepository.findAll();
		}
		return entity;
	}
	
	@GetMapping("/{id}")
	public Set<?> findOne(@PathVariable Long id) {
		return Collections.singleton(entityRepository.findOne(id));
	}

	@PostMapping("")
	public void postOne(@RequestBody Table entity) {
		entityRepository.saveAndFlush(entity);
	}

	@PutMapping("/{id}")
	public void putOne(@PathVariable Long id, @RequestBody Table entity) {
		entity.setId(id);
		entityRepository.saveAndFlush(entity);
	}

	@PatchMapping("/{id}")
	public void patchPart(@PathVariable Long id, @RequestBody Table updateEntity)
			throws JsonProcessingException, IOException {
		Table entity = entityRepository.findOne(id);
		String updateTableString = objectMapper.writeValueAsString(updateEntity);
		entity = objectMapper.readerForUpdating(entity).readValue(updateTableString);
		entityRepository.save(entity);
	}

	@DeleteMapping("/{id}")
	public void deleteOne(@PathVariable Long id) {
		entityRepository.delete(id);
	}

}
