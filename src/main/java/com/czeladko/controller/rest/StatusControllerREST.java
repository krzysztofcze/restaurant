package com.czeladko.controller.rest;

import java.io.IOException;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.Status;
import com.czeladko.repository.StatusRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/status")
@Transactional
public class StatusControllerREST {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private StatusRepository statusRepository;

	@GetMapping("")
	public Collection<Status> findAll(@RequestParam(required = false) String sort) {
		Collection<Status> statuses = null;
		if (sort != null) {
			switch (sort.toLowerCase()) {
			case "asc":
				statuses = statusRepository.findAllByOrderBySortNumberAsc();
				break;

			case "desc":
				statuses = statusRepository.findAllByOrderBySortNumberDesc();
				break;

			default:
				statuses = statusRepository.findAll();
			}
		} else {
			statuses = statusRepository.findAll();
		}
		return statuses;
	}

	@GetMapping("/{id}")
	public Status findOne(@PathVariable Long id) {
		return statusRepository.findOne(id);
	}
	
	@PostMapping("")
	public void postOne(@RequestBody Status status) {
		statusRepository.saveAndFlush(status);
	}
	
	@PutMapping("/{id}")
	public void putOne(@PathVariable Long id,@RequestBody Status status) {
		status.setId(id);
		statusRepository.saveAndFlush(status);
	}
	
	@PatchMapping("/{id}")
	public void patchPart(@PathVariable Long id,@RequestBody Status updateStatus) throws JsonProcessingException, IOException {
		Status status = statusRepository.findOne(id);
		String updateStatusString = objectMapper.writeValueAsString(updateStatus);
		status = objectMapper.readerForUpdating(status).readValue(updateStatusString);
		statusRepository.save(status);
	}
	
	@DeleteMapping("/{id}")
	public void deleteOne(@PathVariable Long id) {
		statusRepository.delete(id);
	}
}
