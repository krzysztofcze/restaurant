package com.czeladko.controller.rest;

import java.io.IOException;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.czeladko.entity.Product;
import com.czeladko.repository.ProductRepository;
import com.czeladko.services.MenuService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/RESTmenu")
@Transactional
public class MenuControllerREST {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MenuService menuService;

	@GetMapping("")
	public Collection<Product> menu(
//	public LinkedHashMap<Category, Collection<Product>> menu(
			@RequestParam(required = false, name = "available", defaultValue = "true") String available) {
		switch (available.toLowerCase()) {
		case "true":
//			return productRepository.findAllByAvailableOrderBySortNumberAscNameAsc(true);
			return productRepository.findAllByOrderBySortNumberAscNameAsc();
//			return menuService.genarate_menu(true);
		case "false":
			return productRepository.findAllByAvailableOrderBySortNumberAscNameAsc(false);
//			return menuService.genarate_menu(false);
		default:
			return productRepository.test();
//			return productRepository.findAllByAvailableOrderBySortNumberAscNameAsc(true);
//			return menuService.genarate_menu(true);
		}
	}

	/*
	 * public Collection<Product> findAll(@RequestParam(required = false) String
	 * sort) { Collection<Product> products = null; if (sort != null) { switch
	 * (sort.toLowerCase()) { case "asc": products =
	 * productRepository.findAllByOrderBySortNumberAscNameAsc(); break;
	 * 
	 * case "desc": products =
	 * productRepository.findAllByOrderBySortNumberAscNameDesc(); break;
	 * 
	 * default: products = productRepository.findAll(); } } else { products =
	 * productRepository.findAll(); } return products; }
	 */

	@GetMapping("/{id}")
	public Product findOne(@PathVariable Long id) {
		return productRepository.findOne(id);
	}

	@PostMapping("")
	public void postOne(@RequestBody Product producs) {
		productRepository.saveAndFlush(producs);
	}

	@PutMapping("/{id}")
	public void putOne(@PathVariable Long id, @RequestBody Product product) {
		product.setId(id);
		productRepository.saveAndFlush(product);
	}

	@PatchMapping("/{id}")
	public void patchPart(@PathVariable Long id, @RequestBody Product updateProduct)
			throws JsonProcessingException, IOException {
		Product product = productRepository.findOne(id);
		String updateProductString = objectMapper.writeValueAsString(updateProduct);
		product = objectMapper.readerForUpdating(product).readValue(updateProductString);
		productRepository.save(product);
	}

	@DeleteMapping("/{id}")
	public void deleteOne(@PathVariable Long id) {
		productRepository.delete(id);
	}

	/*
	 * @Autowired private CustomerRepository customerRepository;
	 * 
	 * @Autowired private CustomerOrderRepository customerOrderRepository;
	 * 
	 * @Autowired private TableRepository tableRepository;
	 * 
	 * @Autowired private CustomerOrderService customerOrderService;
	 * 
	 * @Autowired private MenuService menuService;
	 * 
	 * @Autowired private CategoryRepository categoryRepository;
	 * 
	 * @GetMapping("") public String home(Model model, HttpSession session,
	 * 
	 * @RequestParam(required = false, name = "available", defaultValue = "true")
	 * boolean available) {
	 * 
	 * LinkedHashMap<Category, Collection<Product>> menu =
	 * menuService.genarate_menu(available); model.addAttribute("menu", menu);
	 * 
	 * Customer customer = new Customer(); model.addAttribute("customer", customer);
	 * 
	 * Collection<Customer> customers = customerRepository.findAll();
	 * model.addAttribute("customers", customers);
	 * 
	 * Collection<CustomerOrder> collectionCustomerOrder =
	 * customerOrderRepository.findAll();
	 * model.addAttribute(collectionCustomerOrder);
	 * 
	 * Table table = new Table(); model.addAttribute("table", table);
	 * 
	 * Collection<Table> collectionTable = tableRepository.findAll();
	 * model.addAttribute("tables", collectionTable);
	 * 
	 * Collection<Category> categories =
	 * categoryRepository.findAllByOrderBySortNumberAscNameAsc();
	 * model.addAttribute("categories",categories); return "pages/menu"; }
	 * 
	 * @GetMapping("/order") public String addToOrder(HttpSession
	 * session, @RequestParam(name = "id") Long id, Model model) {
	 * 
	 * CustomerOrder currentCustomerOrder = customerOrderService.newEmpty(); if
	 * (session.getAttribute("currentCustomerOrder") != null) { currentCustomerOrder
	 * = (CustomerOrder) session.getAttribute("currentCustomerOrder"); }
	 * currentCustomerOrder = customerOrderService.productAdd(currentCustomerOrder,
	 * id); model.addAttribute("currentCustomerOrder", currentCustomerOrder); return
	 * "redirect:/menu/";
	 * 
	 * }
	 * 
	 * @ModelAttribute(name = "categoriesSorted") public Collection<Category>
	 * listCategories() { return menuService.getCategoriesSorted(); }
	 */

}
