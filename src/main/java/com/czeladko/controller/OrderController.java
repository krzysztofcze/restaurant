package com.czeladko.controller;

import java.util.Collection;
import java.util.LinkedList;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.czeladko.entity.CustomerOrder;
import com.czeladko.repository.CustomerOrderRepository;
import com.czeladko.services.CustomerOrderService;

@Controller
@RequestMapping("orders/")
@Transactional
public class OrderController {

	@Autowired
	private CustomerOrderRepository customerOrderRepository;
	
	@Autowired
	private CustomerOrderService customerOrderService;

	@GetMapping("/")
	public String get(HttpSession session, Model model) {
		Collection<CustomerOrder> customerOrders = customerOrderRepository.findAll();
		model.addAttribute("customerOrders", customerOrders);
		return "pages/order";
	}

	@GetMapping("client")
	public String viewClientOrder() {
		
		return "pages/order";
	}

	@GetMapping("kitchen")
	public String viewForKitchen(Model model) {
		LinkedList<CustomerOrder> customerOrders = customerOrderService.viewForKitchen();
		model.addAttribute("customerOrders", customerOrders);
		return "pages/order";
	}

	@GetMapping("waitress")
	public String viewForWaitres(Model model) {
		LinkedList<CustomerOrder> customerOrders = customerOrderService.viewForWaitress();
		model.addAttribute("customerOrders", customerOrders);
		return "pages/order";
	}

	@GetMapping("cancel")
	public String cancelOrder(@RequestParam(name = "id") Long id, Model model) {
		CustomerOrder currentCustomerOrder = customerOrderRepository.findOne(id);
		customerOrderService.cancelByKitchen(currentCustomerOrder);
		return "redirect:/orders/";
	}
	
	@GetMapping("next")
	public String next(@RequestParam(name = "id") Long id, Model model) {
		CustomerOrder currentCustomerOrder = customerOrderRepository.findOne(id);
		customerOrderService.next(currentCustomerOrder);
		return "redirect:/orders/";
	}
}