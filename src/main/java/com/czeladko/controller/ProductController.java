package com.czeladko.controller;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.czeladko.entity.Category;
import com.czeladko.entity.Product;
import com.czeladko.repository.CategoryRepository;
import com.czeladko.repository.ProductRepository;

@Controller
@RequestMapping({ "/products/", "/product/" })
@Transactional
public class ProductController {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	private Collection<Category> categoryFindAllMine() {
		return categoryRepository.findAllByOrderBySortNumberAscNameAsc();
	}

	@GetMapping("")
	public String showAll(Model model, @RequestParam(name = "edit", required = false) Long id) {
		if (id != null) {
			if (id == 0) {
				Product product = new Product();
				model.addAttribute("product", product);
			} else {
				Product product = new Product();
				product = productRepository.findOne(id);
				model.addAttribute("product", product);
			}
		}
		Collection<Product> products = productRepository.findAllByOrderBySortNumberAscNameAsc();
		model.addAttribute("products", products);
		return "pages/product";
	}

	@GetMapping("move")
	public ModelAndView move(Model model, @RequestParam(name = "move", required = false) Integer move,
			@RequestParam(name = "id", required = false) Long id) {
		Product product = productRepository.findOne(id);
		product.setSortNumber(product.getSortNumber() + move);
		productRepository.save(product);
		Collection<Product> products = productRepository.findAllByOrderBySortNumberAscNameAsc();
		model.addAttribute("products", products);
		return new ModelAndView("redirect:/product/");
	}

	@GetMapping("delete")
	public ModelAndView move(Model model, @RequestParam(name = "id", required = false) Long id) {
		if (id != null && productRepository.exists(id)) {
			productRepository.delete(id);
		}
		Product product = new Product();
		model.addAttribute("product", product);
		Collection<Product> products = productRepository.findAllByOrderBySortNumberAscNameAsc();
		model.addAttribute("products", products);
		return new ModelAndView("redirect:/product/");
	}

	@PostMapping("")
	public ModelAndView addOrEdit(@ModelAttribute Product product) {
		productRepository.save(product);
		return new ModelAndView("redirect:/product/");
	}
	
	@ModelAttribute(name="categories")
	public Collection<Category> listCategories() {
		Collection<Category> categories = categoryFindAllMine();
		return categories;
	}

}
