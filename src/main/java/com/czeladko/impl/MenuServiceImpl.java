package com.czeladko.impl;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.czeladko.entity.Category;
import com.czeladko.entity.Product;
import com.czeladko.repository.CategoryRepository;
import com.czeladko.repository.ProductRepository;
import com.czeladko.services.MenuService;

@Component
public class MenuServiceImpl implements MenuService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	private Collection<Category> categoryFindAllMine(boolean visible) {
		return categoryRepository.findAllByVisibleOrderBySortNumberAscNameAsc(visible);
	}

	private Collection<Product> productFindAllMineByCatAndAvailable(Category category, boolean available) {
		return productRepository.findAllByCategoryAndAvailableOrderBySortNumberAscNameAsc(category, available);
	}

	/* (non-Javadoc)
	 * @see com.czeladko.impl.MenuService#genarate_menu(boolean)
	 */
	@Override
	public LinkedHashMap<Category, Collection<Product>> genarate_menu(boolean available) {
		LinkedHashMap<Category, Collection<Product>> menu = new LinkedHashMap<>();
		if (available == false) {
			Collection<Category> categories = getCategoriesSorted();
			Collection<Product> listaProduktow;
			for (Category cat : categories) {
				listaProduktow = productRepository.findAllByCategoryOrderBySortNumberAscNameAsc(cat);
				menu.put(cat, listaProduktow);
			}
		} else if (available == true) {
			Collection<Category> categories = categoryFindAllMine(available);
			Collection<Product> listaProduktow;
			for (Category cat : categories) {
				listaProduktow = productFindAllMineByCatAndAvailable(cat, available);
				menu.put(cat, listaProduktow);
			}
		}
		return menu;
	}
	
	/* (non-Javadoc)
	 * @see com.czeladko.impl.MenuService#getCategoriesSorted()
	 */
	@Override
	public Collection<Category> getCategoriesSorted(){
		return categoryRepository.findAllByOrderBySortNumberAscNameAsc();
	}
}
