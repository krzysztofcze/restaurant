package com.czeladko.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.czeladko.entity.Customer;
import com.czeladko.repository.CustomerRepository;
import com.czeladko.services.CustomerService;

@Component
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private static final String AT_NAMEOFOURCOMPANY_PL = "@czeladko.com";

	@Autowired
	private CustomerRepository customerRepository;

	private boolean checkPass(String plainPassword, String hashedPassword) {
		return BCrypt.checkpw(plainPassword, hashedPassword);
	}

	@Override
	public boolean login(String email, String password) {
		Customer customer = customerRepository.findCustomerByEmail(email);
		if (customer != null) {
			Customer customerFromDb = customerRepository.findOne(customer.getId());
			boolean cp = checkPass(password, customerFromDb.getPassword());
			if (cp) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Customer createNewAnonymouseCustomer() {
		Customer customer = new Customer();

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String dateString = dateFormat.format(date);

		String name = "Client_" + dateString + "_1";
		for (; customerExistWithEmail(name + AT_NAMEOFOURCOMPANY_PL);) {
			String lastName = findLastAnonymousClientUIDNumber(name + AT_NAMEOFOURCOMPANY_PL).getName();
			String[] lastNameSplited = lastName.split("_");
			int lastCustomerId = Integer.parseInt(lastNameSplited[lastNameSplited.length - 1]);
			lastCustomerId += 1;
			lastNameSplited[lastNameSplited.length - 1] = String.valueOf(lastCustomerId);
			String newName = lastNameSplited[0] + "_" + lastNameSplited[1] + "_" + lastNameSplited[2];
			name = newName;
		}
		customer.setName(name);
		customer.setPassword(name);
		customer.setEmail((name + AT_NAMEOFOURCOMPANY_PL));
		customerRepository.save(customer);
		return customer;
	}

	@Override
	public boolean customerExistWithEmail(String email) {
		return customerRepository.findCustomerByEmail(email) != null;
	}

	/**
	 * finding last email for anonymous customer with UID number for generate the
	 * new anonymous customer UID
	 * 
	 * @param email
	 *            address ( unique in DB )
	 * @return Customer entity with last anonymous customer
	 */
	private Customer findLastAnonymousClientUIDNumber(String email) {
		return customerRepository.findCustomerFirstByEmailOrderByEmailDesc(email);
	}
}
