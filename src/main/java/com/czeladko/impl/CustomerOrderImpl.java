package com.czeladko.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.czeladko.entity.CustomerOrder;
import com.czeladko.entity.OrderProduct;
import com.czeladko.entity.Product;
import com.czeladko.entity.Status;
import com.czeladko.repository.CustomerOrderRepository;
import com.czeladko.repository.OrderProductRepository;
import com.czeladko.repository.ProductRepository;
import com.czeladko.repository.StatusRepository;
import com.czeladko.services.CustomerOrderService;

@Service
public class CustomerOrderImpl implements CustomerOrderService {

	private static final int NUMBER_ONE = 1;
	private static final long STATUS_FOR_NEW_EMPTY_ORDER = 1L;
	private static final long STATUS_FOR_ORDERED_ORDER = 2L;
	private static final long STATUS_FOR_ORDER_CANCELED_BY_CLIENT = 9L;
	private static final long STATUS_FOR_ORDER_CANCELED_BY_KITCHEN = 10L;
	
	@Autowired
	private CustomerOrderRepository customerOrderRepository;

	@Autowired
	private StatusRepository statusRepository;

	@Autowired
	private OrderProductRepository orderProductRepository;

	@Autowired
	private ProductRepository productRepository;

	public CustomerOrder accept(CustomerOrder currentCustomerOrder) {
		Status status = statusRepository.findOne(STATUS_FOR_ORDERED_ORDER);
		currentCustomerOrder.setStatus(status);
		customerOrderRepository.save(currentCustomerOrder);
		return currentCustomerOrder;
	}

	public CustomerOrder cancelByCustomer(CustomerOrder currentCustomerOrder) {
		if (isCustomerOrderEmpty(currentCustomerOrder)) {
		} else {
			Status status = statusRepository.findOne(STATUS_FOR_ORDER_CANCELED_BY_CLIENT);
			currentCustomerOrder.setStatus(status);
			customerOrderRepository.saveAndFlush(currentCustomerOrder);
			currentCustomerOrder = newEmpty();
		}
		return currentCustomerOrder;
	}

	public boolean isCustomerOrderEmpty(CustomerOrder currentCustomerOrder) {
		return (currentCustomerOrder.getOrderProduct() == null);
	}

	public CustomerOrder cancelByKitchen(CustomerOrder currentCustomerOrder) {
		Status status = statusRepository.findOne(STATUS_FOR_ORDER_CANCELED_BY_KITCHEN);
		currentCustomerOrder.setStatus(status);
		customerOrderRepository.saveAndFlush(currentCustomerOrder);
		return currentCustomerOrder;
	}

	public CustomerOrder next(CustomerOrder currentCustomerOrder) {
		Status currentStatus = currentCustomerOrder.getStatus();
		ArrayList<Status> statuses = (ArrayList<Status>) statusRepository.findAllByOrderBySortNumberAsc();
		Integer index = 0;
		Integer size = statuses.size();
		for (Status status : statuses) {
			if (currentStatus.getId().equals(status.getId())) {
				index = statuses.indexOf(status);
			}
		}
		if (index == size - 1) {

		} else {
			index++;
		}
		currentStatus = statuses.get(index);
		currentCustomerOrder.setStatus(currentStatus);
		customerOrderRepository.saveAndFlush(currentCustomerOrder);
		return currentCustomerOrder;
	}

	public CustomerOrder newEmpty() {
		Status status = statusRepository.findOne(STATUS_FOR_NEW_EMPTY_ORDER);
		CustomerOrder currentCustomerOrder = new CustomerOrder();
		currentCustomerOrder.setStatus(status);
		return currentCustomerOrder;
	}

	public CustomerOrder updateQuantity(CustomerOrder currentCustomerOrder, Long id, Integer change) {
		List<OrderProduct> listOrderProduct = currentCustomerOrder.getOrderProduct();
		for (OrderProduct orderProduct : listOrderProduct) {
			if (orderProduct.getProduct().getId().equals(id)) {
				Integer temp = orderProduct.getQuantity() + change;
				if (temp >= 0) {
					orderProduct.setQuantity(temp);
					orderProductRepository.saveAndFlush(orderProduct);
				}
			}
		}
		return currentCustomerOrder;
	}

	public CustomerOrder productAdd(CustomerOrder currentCustomerOrder, Long productId) {
		List<OrderProduct> listOrderedProducts = new ArrayList<OrderProduct>();
		if (currentCustomerOrder.getOrderProduct() != null) {
			listOrderedProducts = currentCustomerOrder.getOrderProduct();
			boolean productNotInListOrderedProducts = true;
			for (OrderProduct op : listOrderedProducts) {
				if (op.getProduct().getId().equals(productId)) {
					op.setQuantity(op.getQuantity() + 1);
					orderProductRepository.saveAndFlush(op);
					productNotInListOrderedProducts = false;
				}
			}
			if (productNotInListOrderedProducts) {
				Product newProduct = productRepository.findOne(productId);
				OrderProduct newOrderProduct = new OrderProduct(newProduct, NUMBER_ONE);
				orderProductRepository.saveAndFlush(newOrderProduct);
				listOrderedProducts.add(newOrderProduct);
			}
		} else {
			Product newProduct = productRepository.findOne(productId);
			OrderProduct newOrderProduct = new OrderProduct(newProduct, NUMBER_ONE);
			orderProductRepository.saveAndFlush(newOrderProduct);
			listOrderedProducts.add(newOrderProduct);
		}
		currentCustomerOrder.setOrderProduct(listOrderedProducts);
		customerOrderRepository.saveAndFlush(currentCustomerOrder);

		return currentCustomerOrder;
	}

	public CustomerOrder productRemove(CustomerOrder currentCustomerOrder, Long productId) {
		List<OrderProduct> listOrderProduct = currentCustomerOrder.getOrderProduct();
		OrderProduct orderProductToRemove = null;
		for (OrderProduct orderProduct : listOrderProduct) {
			if (orderProduct.getProduct().getId().equals(productId)) {
				orderProductToRemove = orderProduct;
			}
		}
		listOrderProduct.remove(orderProductToRemove);
		currentCustomerOrder.setOrderProduct(listOrderProduct);
		customerOrderRepository.saveAndFlush(currentCustomerOrder);
		return currentCustomerOrder;
	}

	public LinkedList<CustomerOrder> viewForKitchen() {
		return view(viewStatusesIdForKitchen);
	}

	public LinkedList<CustomerOrder> viewForWaitress() {
		return view(viewStatusesIdForWaitress);
	}

	// 2,5,7 Long id of status for kitchen view
	// 1,2,5,7,6,8,9,10 Long id for watress
	private Long viewStatusesIdForKitchen[] = { 5L, 7L, 2L };
	private Long viewStatusesIdForWaitress[] = { 1L, 2L, 5L, 7L, 6L, 8L, 9L, 10L };

	private LinkedList<CustomerOrder> view(Long[] listWithThoseStatuses) {
		Collection<CustomerOrder> listCustomerOrders = customerOrderRepository.findAll();
		LinkedList<CustomerOrder> list = new LinkedList<CustomerOrder>();
		for (CustomerOrder customerOrder : listCustomerOrders) {
			for (Long id : listWithThoseStatuses) {
				if (customerOrder.getStatus().getId() == id) {
					list.add(customerOrder);
				}
			}
		}
		return list;
	}
}
