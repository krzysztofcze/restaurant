package com.czeladko.webapp;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
@ComponentScan
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		web
			.ignoring()
			.antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**","/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html", "/webjars/**");
	}
	
	//https://spring.io/blog/2015/06/08/cors-support-in-spring-framework
	//https://docs.spring.io/spring-security/site/docs/current/reference/html/cors.html
		@Bean
		CorsConfigurationSource corsConfigurationSource() {
			CorsConfiguration configuration = new CorsConfiguration();
			configuration.setAllowedOrigins(Arrays.asList("*","http://czk.myftp.org"));
			configuration.setAllowedMethods(Arrays.asList("GET","POST","PATCH","PUT","DELETE"));
			configuration.setAllowedHeaders(Arrays.asList("*"));
			configuration.setAllowCredentials(true);
			configuration.setMaxAge(3600L);
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			source.registerCorsConfiguration("/**", configuration);
			return source;
		}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf()
			.ignoringAntMatchers("/category**")
			.ignoringAntMatchers("/category/**");

		//ROLES:
		//"CUSTOMER" NOT LOGIN NEEDED
		//"ADMIN" - site admin 
		//"BOSS" - can add new user and set roles
		//"MANAGERS" - user Manager can see all , but cant change roles
		//"KITCHEN" - user view kitchen only
		//"WAITRESS" - user view waitress only
		http.authorizeRequests()
			.antMatchers("/products/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/categories/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/statuses/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/tables/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/customers/**").hasAnyRole("USER","ADMIN")
			.antMatchers("/orders/**").hasAnyRole("USER","ADMIN")
//			.antMatchers("/users/**").hasRole("USER")// USER role can access /users/**
//			.antMatchers("/admin/**").hasRole("ADMIN")// ADMIN role can access /admin/**
			.antMatchers("/**").permitAll()// anyone can access /**
			.antMatchers("/frontend/**").permitAll()
			.anyRequest().authenticated()// any other request just need authentication
			.and()
			.cors().and()//ADD CORS 
			.formLogin()// enable form login default for spring security
			.defaultSuccessUrl("/", false); //redirect after success login to page before authentication  
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("tim").password("123").roles("ADMIN")
			.and()
			.withUser("joe").password("234").roles("USER")
			.and()
			.withUser("admin").password("admin").roles("ADMIN");
	}
	
	@Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
