package com.czeladko.webapp;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import com.czeladko.sitemesh.filter.MySitemeshFilter;

public class AppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(final ServletContext servletContext) throws ServletException {
		final AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		/* ===START=== SOLVE PROBLEM WITH ENCODING TO DB WHEN SAVING THE FORM POST */
		/*
		 * SOURCE
		 * https://stackoverflow.com/questions/20863489/characterencodingfilter-dont-
		 * work-together-with-spring-security-3-2-0
		 */
		FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encoding-filter",
				new CharacterEncodingFilter());
		encodingFilter.setInitParameter("encoding", "UTF-8");
		encodingFilter.setInitParameter("forceEncoding", "true");
//		encodingFilter.addMappingForUrlPatterns(null, true, "/*");
		/* ===END=== SOLVE PROBLEM WITH ENCODING TO DB WHEN SAVING THE FORM POST */
		ctx.register(AppConfig.class);
		ctx.setServletContext(servletContext);
		
		addSitemeshFilterToServletContext(servletContext);
		
		final ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher",
				new DispatcherServlet(ctx));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}

	private void addSitemeshFilterToServletContext(ServletContext servletContext) {
		FilterRegistration.Dynamic sitemesh = servletContext.addFilter("sitemesh", new MySitemeshFilter());
		EnumSet<DispatcherType> sitemeshDispatcherTypes = EnumSet.of( DispatcherType.REQUEST,
				DispatcherType.FORWARD,DispatcherType.ERROR, DispatcherType.INCLUDE, DispatcherType.ASYNC);
//		,DispatcherType.ERROR, DispatcherType.INCLUDE, DispatcherType.ASYNC
		sitemesh.addMappingForUrlPatterns(sitemeshDispatcherTypes, true, "/*");
	}

}