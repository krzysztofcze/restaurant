package com.czeladko.webapp;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class WebSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
	public WebSecurityInitializer() {
		super(WebSecurityConfig.class);
	}
}
