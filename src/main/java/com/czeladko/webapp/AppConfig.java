package com.czeladko.webapp;

import java.util.Locale;

import javax.persistence.EntityManagerFactory;
import javax.servlet.FilterRegistration;
import javax.validation.Validator;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.AntPathMatcher;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.LocaleContextResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/*Określamy naszą klasę jako element konfiguracji.*/
@Configuration
/* Adnotacja importuje konfigurację, którą możemy nadpisywać w klasie. */
@EnableWebMvc
/* Scanowanie componentow w package pl.coderslab */
@ComponentScan(basePackages = "com.czeladko")
/* Włącza zarządzanie transakcjami. */
@EnableTransactionManagement
/* Włącza Spring Data NO MORE DAO */
/* @EnableJpaRepositories (basePackageClasses = PersonRepository.class) */
@EnableJpaRepositories(basePackages = { "com.czeladko.repository" })
/*
 * Dzięki temu, że rozszerzamy klasę WebMvcConfigurerAdapter nie musimy
 * nadpisywać wszystkich metod interfejsu, który ona rozszerza.
 */
/*@EnableSwagger2
@Import(SwaggerConfig.class)*/
public class AppConfig extends WebMvcConfigurerAdapter {

	// case sensitive adress url
	/*
	 * (non-Javadoc) This one is added by me (k.cz.) to have one address that dont
	 * depend on CaseSensitive in url
	 * 
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#
	 * configurePathMatch(org.springframework.web.servlet.config.annotation.
	 * PathMatchConfigurer)
	 */
	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		AntPathMatcher matcher = new AntPathMatcher();
		matcher.setCaseSensitive(false);
		configurer.setPathMatcher(matcher);
	}

	@Bean
	public ViewResolver viewResolver() {
		final InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	/*
	 * Bez tego ustawienia dla zasobów statycznych (obrazki, js, css) Spring
	 * próbowałby dopasować metodę kontrolera, która pasowałaby np. do adresu
	 * /logo.jpg
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	//http://www.baeldung.com/spring-mvc-static-resources
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
			.addResourceHandler("/resources/**")
			/*.addResourceLocations("/WEB-INF/views/resources/");*/
			.addResourceLocations("classpath:/WEB-INF/views/resources/","/WEB-INF/views/resources/");

		
		 // ===== SWAGGER SPRING FOX ====
//		 registry
//		 	.addResourceHandler("swagger-ui.html")
//		 	.addResourceLocations("classpath:/META-INF/resources/");
//		 registry
//		 	.addResourceHandler("/webjars/**")
//		 	.addResourceLocations("classpath:/META-INF/resources/webjars/");
		 
		 /*registry.addResourceHandler("/documentation/**").addResourceLocations(
		 "classpath:/META-INF/resources/");*/
		 
		// ===== SITE MESH ====
		registry
			.addResourceHandler("/WEB-INF/views/decorators/**")
			.addResourceLocations("/WEB-INF/views/decorators/");
		registry
			.addResourceHandler("/WEB-INF/views/resources/**")
			.addResourceLocations("/WEB-INF/views/resources/");
		registry
			.addResourceHandler("/frontend/**")
			.addResourceLocations("/WEB-INF/views/frontend/","classpath:/WEB-INF/views/frontend/");
		
	}

	/*
	 * Pierwsze ziarno definiuje fabrykę EntityManagera (zarządcy encji) - obiektu,
	 * którym będziemy się posługiwać w celu wykonywania operacji na naszych
	 * encjach.
	 */
	@Bean
	public LocalEntityManagerFactoryBean entityManagerFactory() {
		LocalEntityManagerFactoryBean emfb = new LocalEntityManagerFactoryBean();
		/*
		 * Nazwa jednostki utrwalania musi być taka sama jak ustalona wcześniej w pliku
		 * persistence.xml
		 */
		emfb.setPersistenceUnitName("restaurant");
		return emfb;
	}

	/*
	 * Określamy również sposób zarządzania transakcjami - włączamy zarządzanie
	 * transakcjami przez Springa.
	 */
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager tm = new JpaTransactionManager(emf);
		return tm;
	}

	/*
	 * Możemy zdefiniować pliki tłumaczeń dla konkretnych języków. W tym celu
	 * tworzymy plik ValidationMessages_pl.properties w lokalizacji:
	 * src/main/resources Spring automatycznie skorzysta z odpowiedniego pliku w
	 * zależności od od ustawień lokalizacyjnych. Aby ustawić domyślny język Polski
	 * należy w klasie konfiguracji zdefiniować ziarno Springa:
	 */
	@Bean(name = "localeResolver")
	public LocaleContextResolver getLocaleContextResolver() {
		final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(new Locale("pl", "PL"));
		return localeResolver;
	}

	@Bean
	public Validator validator() {
		return new LocalValidatorFactoryBean();
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/i18n/language");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver = new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("pl"));
		resolver.setCookieName("myLanguage");
		resolver.setCookieMaxAge(4800);
		return resolver;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("myLanguage");
		registry.addInterceptor(interceptor);
	}

	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	// http://www.baeldung.com/spring-cors
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				//.allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH") 
				.allowedMethods("*")
				.allowedOrigins("*","http://czk.myftp.org")
				.allowedHeaders("*");
	}
	
/*	@Bean
	public FilterRegistration corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		return null;
	}*/
	
}