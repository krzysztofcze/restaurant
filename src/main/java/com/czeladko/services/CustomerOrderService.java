package com.czeladko.services;

import java.util.LinkedList;

import org.springframework.stereotype.Component;

import com.czeladko.entity.CustomerOrder;

@Component
public interface CustomerOrderService {

	CustomerOrder accept(CustomerOrder currentCustomerOrder);

	CustomerOrder cancelByCustomer(CustomerOrder currentCustomerOrder);

	boolean isCustomerOrderEmpty(CustomerOrder currentCustomerOrder);

	CustomerOrder cancelByKitchen(CustomerOrder currentCustomerOrder);

	CustomerOrder next(CustomerOrder currentCustomerOrder);

	CustomerOrder newEmpty();

	CustomerOrder updateQuantity(CustomerOrder currentCustomerOrder, Long id, Integer change);

	CustomerOrder productAdd(CustomerOrder currentCustomerOrder, Long productId);

	CustomerOrder productRemove(CustomerOrder currentCustomerOrder, Long productId);

	LinkedList<CustomerOrder> viewForKitchen();

	LinkedList<CustomerOrder> viewForWaitress();

}