package com.czeladko.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.czeladko.entity.OrderProduct;
import com.czeladko.repository.OrderProductRepository;


@Component
public class OrderProductService extends OrderProduct {

	@Autowired
	OrderProductRepository orderProductRepository;
	
}
