package com.czeladko.services;

import java.util.Collection;
import java.util.LinkedHashMap;

import org.springframework.stereotype.Component;

import com.czeladko.entity.Category;
import com.czeladko.entity.Product;

@Component
public interface MenuService {

	LinkedHashMap<Category, Collection<Product>> genarate_menu(boolean available);

	Collection<Category> getCategoriesSorted();

}