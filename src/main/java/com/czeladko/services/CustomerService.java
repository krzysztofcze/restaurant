package com.czeladko.services;

import org.springframework.stereotype.Component;

import com.czeladko.entity.Customer;

@Component
public interface CustomerService {

	Customer createNewAnonymouseCustomer();
	
	boolean login(String email, String password);
	
	/**
	 * @param email address ( unique in DB ) 
	 * @return 
	 * true if customer exist in DB, 
	 * return false if customer doesn't exist in DB
	 */
	boolean customerExistWithEmail(String email);
}