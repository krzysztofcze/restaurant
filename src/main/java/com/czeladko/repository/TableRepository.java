package com.czeladko.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.Table;

@Repository
public interface TableRepository extends JpaRepository<Table, Long> {
	Collection<Table> findAllByOrderBySortNumberAsc();
	Collection<Table> findAllByOrderBySortNumberDesc();
}
