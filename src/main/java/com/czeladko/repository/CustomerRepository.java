package com.czeladko.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long>{
	Customer findCustomerByEmail(String email);
	Customer findCustomerFirstByEmailOrderByEmailDesc(String name);
}
