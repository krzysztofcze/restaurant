package com.czeladko.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.Status;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
	Collection<Status> findAllByOrderBySortNumberAsc();
	Collection<Status> findAllByOrderBySortNumberDesc();
}
