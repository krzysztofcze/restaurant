package com.czeladko.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}