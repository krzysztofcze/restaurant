package com.czeladko.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

	Collection<Category> findAllByOrderBySortNumberAscNameAsc();
	Collection<Category> findAllByOrderBySortNumberAscNameDesc();
	
	Collection<Category> findAllByVisibleOrderBySortNumberAscNameAsc(boolean visible);
	Collection<Category> findAllByVisibleOrderBySortNumberAscNameDesc(boolean visible);
	
	Collection<Category> findAllByOrderBySortNumberAsc();
	Collection<Category> findAllByOrderBySortNumberDesc();
	
}

