package com.czeladko.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.czeladko.entity.Category;
import com.czeladko.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	Collection<Product> findAllByOrderBySortNumberAscNameAsc();
	Collection<Product> findAllByOrderBySortNumberAscNameDesc();
	
	Collection<Product> findAllByCategoryOrderBySortNumberAscNameAsc(Category category);
	Collection<Product> findAllByCategoryOrderBySortNumberAscNameDesc(Category category);
	
	Collection<Product> findAllByAvailableOrderBySortNumberAscNameAsc(boolean available);
	Collection<Product> findAllByAvailableOrderBySortNumberAscNameDesc(boolean available);
	
	Collection<Product> findAllByCategoryAndAvailableOrderBySortNumberAscNameAsc(Category category,boolean available);
	Collection<Product> findAllByCategoryAndAvailableOrderBySortNumberAscNameDesc(Category category,boolean available);
	default Collection<Product> test(){
		
		return findAllByOrderBySortNumberAscNameAsc();
	}
	
	/*
	SELECT p.available,p.id prod_id,p.name prod_name,
	p.sortNumber prod_sort,p.category_id prod_cat,
	c.id cat_id,c.sortNumber cat_sort,c.name cat_name FROM product p
	INNER JOIN category c ON p.category_id=c.id  
	WHERE p.available=1
	ORDER BY `cat_sort` ASC, `cat_name` ASC, `prod_sort` ASC
	*/
	
	/*
	 SELECT * FROM product p INNER JOIN category c ON p.category_id=c.id 
	 WHERE p.available=1 ORDER BY c.sortNumber ASC, c.name ASC, p.sortNumber ASC
	 */
}
