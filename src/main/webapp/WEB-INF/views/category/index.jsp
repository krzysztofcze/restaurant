<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:message code="categories.title" /></title>
</head>
<body>
	<div class="container">
		<div class="container header">
			<h3>header</h3>
			<%@ include file="/WEB-INF/views/header.jsp"%>
		</div>

		<div class="container body">
			<hr>
			<div class="row">
				<table
					class="table-striped table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th><s:message code="category.name" /></th>
							<th><s:message code="category.description" /></th>
							<th><s:message code="category.visible" /></th>
							<th>DB.ID</th>
							<th><s:message code="action" /></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${categories}" var="item">
							<tr>
								<td>${item.sortNumber}</td>
								<td>${item.name}</td>
								<td>${item.description}</td>
								<td>${item.visible}</td>
								<td>${item.id}</td>
								<td>
									<table>
										<tr>
											<td><a class="btn btn-default" href="up?id=${item.id}">
													<span class="glyphicon glyphicon-arrow-up"
													aria-hidden="true"></span>
											</a></td>
											<td><a class="btn btn-warning"
												href="edit?edit_id=${item.id}"> <span
													class="glyphicon glyphicon-edit" aria-hidden="true"></span>
											</a></td>
										</tr>
										<tr>
											<td><a class="btn btn-default" href="down?id=${item.id}">
													<span class="glyphicon glyphicon-arrow-down"
													aria-hidden="true"></span>
											</a></td>
											<td><a class="btn btn-danger"
												href="delete?delete_id=${item.id}"> <span
													class="glyphicon glyphicon-remove" aria-hidden="true"></span>
											</a></td>
										</tr>
									</table>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<hr>
			</div>
		</div>

		<div class="container footer">
			<h3>footer</h3>
		</div>
	</div>
</body>
</html>