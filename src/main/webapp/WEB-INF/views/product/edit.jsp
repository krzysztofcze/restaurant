<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:message code="product.add.title" /></title>
</head>
<body>

	<%@ include file="/WEB-INF/views/header.jsp"%>

	<h1>
		<s:message code="product.edit" />
	</h1>
	<div>
		<f:form action="" method="POST" modelAttribute="product">
			<label><s:message code="product.id" /><f:hidden path="id" />
			</label>
			<label> <s:message code="product.name" /> <f:input
					path="name" />
			</label>
			<label> <s:message code="product.description" /> <f:input
					path="description" />
			</label>
			<label> <s:message code="product.price" /> <f:input
					type="number" step="0.01" min="1" path="price" />
			</label>
			<label> <s:message code="product.available" /> <f:checkbox
					path="available" />
			</label>
			<label> <s:message code="product.sortNumber" /> <f:input
					type="number" path="sortNumber" />
			</label>
			<label> <input type="submit"
				value="<s:message code='product.save'/>" />
			</label>
		</f:form>
	</div>
	<div>
		<hr>
	</div>
	<div>
		<table>
			<thead>
				<tr>
					<td><s:message code="product.id" /></td>
					<td><s:message code="product.name" /></td>
					<td><s:message code="product.description" /></td>
					<td><s:message code="product.price" /></td>
					<td><s:message code="product.available" /></td>
					<td><s:message code="product.sortNumber" /></td>
					<td><s:message code="product.allergent" /></td>
					<td><s:message code="product.photoUrl" /></td>
					<td><s:message code="product.videoUrl" /></td>
					<td><s:message code="product.action" /></td>
				</tr>

			</thead>
			<tbody>
				<c:forEach items="${products}" var="item">
					<c:if test="${item.available==true}">
						<tr>
							<td>${item.id}</td>
							<td>${item.name}</td>
							<td>${item.description}</td>
							<td>${item.price}</td>
							<td>${item.available}</td>
							<td>${item.sortNumber}</td>
							<td>${item.allergent}</td>
							<td>${item.photoUrl}</td>
							<td>${item.videoUrl}</td>
							<td><a href="?order=${item.id}">
									<button class="btn btn-info">
										<s:message code="product.action.order" />
									</button>
							</a> <a href="?edit=${item.id}">
									<button class="btn btn-info">
										<s:message code="product.action.edit" />
									</button>
							</a> <a href="?save=${item.id}">
									<button class="btn btn-info">
										<s:message code="product.action.save" />
									</button>
							</a> <a href="?delete=${item.id}">
									<button class="btn btn-info">
										<s:message code="product.action.delete" />
									</button>
							</a></td>
						</tr>
					</c:if>
				</c:forEach>
			</tbody>

		</table>

	</div>
</body>
</html>