<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Statuses</title>
</head>
<body>
	<div class="container">
		<div class="container header">
			<h3>header</h3>
			<%@ include file="/WEB-INF/views/header.jsp"%>
		</div>

		<div class="container body">

			<c:if test="${status!=null}">
				<div class="row">
					<hr>
					<f:form class="form-inline" action="${post_url}" method="POST"
						modelAttribute="status">
						<div class="form-group">
							<label>Name</label>
							<f:input path="name" type="text" class="form-control" />
						</div>
						<div class="form-group">
							<label>Numer porządkowy</label>
							<f:input type="number" class="form-control" path="sortNumber" />
						</div>
						<div class="form-inline">
							<label>Description</label>
							<f:textarea path="description" class="form-control" cols="69"
								rows="3"></f:textarea>
						</div>
						<button type="submit" class="btn btn-default">Submit</button>
					</f:form>
				</div>
			</c:if>
			<hr>

			<div class="row">

				<a class="btn btn-success" href="?edit=0"> Create new status <span
					class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</a>

			</div>

			<hr>
			<table
				class="table-striped table-hover table-condensed table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Description</th>
						<th>DB.ID</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${statuses}">
						<tr>
							<td>${item.sortNumber}</td>
							<td>${item.name}</td>
							<td>${item.description}</td>
							<td>${item.id}</td>
							<td>
								<table>
									<tr>
										<td><a class="btn btn-default"
											href="move?move=-1&id=${item.id}"> <span
												class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
										</a></td>
										<td><a class="btn btn-warning" href="?edit=${item.id}">
												<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
										</a></td>
									</tr>
									<tr>
										<td><a class="btn btn-default"
											href="move?move=+1&id=${item.id}"> <span
												class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
										</a></td>
										<td><a class="btn btn-danger" href="delete?id=${item.id}">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</a></td>
									</tr>
								</table>
							</td>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<hr>

		<div class="container footer">
			<h3>footer</h3>
		</div>
	</div>
</body>
</html>