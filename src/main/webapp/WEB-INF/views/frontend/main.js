const app = new Vue({
  el: "#app",

  data: {
    form: {},
    addCategory: false,
    editCategory: null,
    category: {},
    categories: [],
    editMenu: null,
    menus: [],
    orders: [],
    products: [],
    statuses: [],
    tables: []
  },
  methods: {
    async deleteMenu(id, i) {
      fetch("http://localhost:8081/Restaurant/RESTmenu/" + id, {
        method: "DELETE"
      })
      .then(() => {
        this.menus.splice(i, 1);
      })
    },
    async updateMenu(menu) {
      fetch("http://localhost:8081/Restaurant/RESTmenu/" + menu.id, {
        body: JSON.stringify(menu),
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        this.editMenu = null;
      })
    },
    createCategory(category){
      event.preventDefault();
      fetch("http://localhost:8081/Restaurant/category", {
        body: JSON.stringify(category),
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        this.addCategory = false
        this.form = {}
        this.getCategory()
      })
    },
    deleteCategory(id, i) {
      if (confirm('Are you sure you want to delete this post?')){
        fetch("http://localhost:8081/Restaurant/category/" + id, {
          method: "DELETE",
        })
        .then(() => {
          this.categories.splice(i, 1);
        })
      }
    },
    updateCategory(category) {
      fetch("http://localhost:8081/Restaurant/category/" + category.id, {
        body: JSON.stringify(category),
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        this.editCategory = null
      })
    },
    getCategory(){
      fetch("http://localhost:8081/Restaurant/category")
      .then(response=>response.json())
      .then((data) => {
        this.categories = data
      })
    }
  },
  mounted() {
    this.getCategory()
  },
  template: `
  <div class="container-fluid mt-12">
    <h1 class="h1"> Restaurant </h1>
    <h3 class="h3"> Categories </h3>
    <table class="table table-striped">

    <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>SortNumber</th>
              <th>Description</th>
              <th>Visible</th>
              <th>Actions</th>
            </tr>
    </thead>

    <tbody>
      <tr v-for="category, i in categories">
        <td>{{category.id}} </td>

        <td>
        <div v-if="editCategory === category.id">
          <input v-on:keyup.13="updateCategory(category)" v-model="category.name" />
          <button v-on:click="updateCategory(category)">save</button>
        </div>
        <div v-else>
          <span v-on:click="editCategory = category.id">{{category.name}}</span>
        </div>
        </td>

        <td>
        <div v-if="editCategory === category.id">
          <input v-on:keyup.13="updateCategory(category)" v-model="category.sortNumber" type="number"/>
          <button v-on:click="updateCategory(category)">save</button>
        </div>
        <div v-else>
          <span v-on:click="editCategory = category.id">{{category.sortNumber}}</span>
        </div>
        </td>

        <td>
        <div v-if="editCategory === category.id">
          <input v-on:keyup.13="updateCategory(category)" v-model="category.description" />
          <button v-on:click="updateCategory(category)">save</button>
        </div>
        <div v-else>
          <span v-on:click="editCategory = category.id">{{category.description}}</span>
        </div>
        </td>

        <td>
        <div v-if="editCategory === category.id">
          <input v-on:keyup.13="updateCategory(category)" v-model="category.visible" type="checkbox" />
          <button v-on:click="updateCategory(category)">save</button>
        </div>
        <div v-else>
          <span v-on:click="editCategory = category.id">{{category.visible}}</span>
        </div>
        </td>

        <td>
        <div>
          <button v-on:click="deleteCategory(category.id, i)">x</button>
        </div>
        </td>
        </tr>
      </tbody>
      </table>
      <hr>
      <button v-on:click="addCategory = !addCategory"> add new category </button>
        <div v-if="addCategory === true">
          <form>
            <input type="text" v-model="form.name" placeholder="name"/>
            <input type="text" v-model="form.description" placeholder="desc"/>
            <input type="number" v-model="form.sortNumber" placeholder="sort number"/>
            <button v-on:click="createCategory(form)">add</button>
          </form>
        {{form.name}} {{form.description}} {{form.sortNumber}}
        </div>

  </div>
  `,
});
