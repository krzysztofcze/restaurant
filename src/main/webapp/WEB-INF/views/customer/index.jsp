<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Customers</title>
</head>
<body>
	<div class="container">
		<div class="container header">
			<h3>header</h3>
			<%@ include file="/WEB-INF/views/header.jsp"%>
		</div>

		<div class="container body">

			<c:if test="${customer!=null}">
				<hr>
				<c:set var="er">
					<f:errors path="*" />
				</c:set>

				<c:if test="${not empty er}">
					<div class="alert alert-error">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<h4>Validation Error :</h4>
						Please correct the following:<br /> ${er}
					</div>
				</c:if>

				<f:form class="form-inline" action="${post_url}" method="POST"
					modelAttribute="customer">
					<div class="form-group">
						<label></label>
						<f:hidden path="id" class="form-control" />
					</div>
					<div class="form-group">
						<label>Name</label>
						<f:input path="name" type="text" class="form-control" />
					</div>
					<div class="form-group">
						<label>Surname</label>
						<f:input path="surname" type="text" class="form-control" />
					</div>

					<div class="form-group">
						<label>Email ( LOGIN )</label>
						<f:input path="email" type="text" class="form-control" />
						<f:errors path="email" cssClass="btn btn-danger"
							element="button"></f:errors>
					</div>
					<div class="form-group">
						<label>Password</label>
						<f:password path="password" class="form-control" min="6"/>
						<f:errors path="password" cssClass="btn btn-danger"
							element="button"></f:errors>
					</div>
					<div class="form-group">
						<label>Phone #</label>
						<f:input path="phoneNumber" type="text" class="form-control" />
					</div>
					<div class="form-group">
						<label>Description</label>
						<f:textarea path="description" class="form-control" cols="69"
							rows="3"></f:textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">
							Submit <span class="glyphicon glyphicon-ok-circle"
								aria-hidden="true"></span>
						</button>
					</div>
				</f:form>
			</c:if>

			<hr>

			<div>

				<a class="btn btn-success"
					href="<c:url value = '/customer/?edit=0' />"> Create new
					customer <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</a>

			</div>
			<hr>
			<table
				class="table-striped table-hover table-condensed table-bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>Surname</th>
						<th>Email(Login)</th>
						<th>PASSWORD</th>
						<th>Phone #</th>
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${customers}">
						<tr>
							<td>${item.name}</td>
							<td>${item.surname}</td>
							<td>${item.email}</td>
							<td>****</td>
							<td>${item.phoneNumber}</td>
							<td>${item.description}</td>

							<td><a class="btn btn-danger"
								href="<c:url value = '/customer/delete?id=${item.id}' />"> <span
									class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> <a
								class="btn btn-warning"
								href="<c:url value = '/customer/?edit=${item.id}' />"> <span
									class="glyphicon glyphicon-edit" aria-hidden="true"></span>
							</a></td>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<hr>

		<div class="container footer">
			<h3>footer</h3>
		</div>
	</div>
</body>
</html>