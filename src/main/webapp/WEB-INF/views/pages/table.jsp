<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<title>table</title>

<html>

<c:if test="${table!=null}">
	<div class="row">
		<hr>
		<f:form class="form-inline" action="${post_url}" method="POST"
			modelAttribute="table">
			<div class="form-group">
				<f:hidden path="id" class="form-control" />
			</div>
			<div class="form-group">
				<label>Name</label>
				<f:input path="name" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>Numer porządkowy</label>
				<f:input type="number" class="form-control" path="sortNumber" />
			</div>
			<div class="form-inline">
				<label>Description</label>
				<f:textarea path="description" class="form-control" cols="69"
					rows="3"></f:textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">
					Submit <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</f:form>
	</div>
</c:if>
<hr>

<div class="row">

	<a class="btn btn-success" href="?edit=0"> Create new table <span
		class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	</a>

</div>

<hr>
<div class="row">
	<p>
		 Sort : <a href="?sort=desc">descending</a> <a href="?sort=asc">ascending</a>
	</p>
	<table class="table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Description</th>
				<th>DB.ID</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${tables}">
				<tr>
					<td>${item.sortNumber}</td>
					<td>${item.name}</td>
					<td>${item.description}</td>
					<td>${item.id}</td>
					<td>
						<table>
							<tr>
								<td><a class="btn btn-default"
									href="move?move=-1&id=${item.id}"> <span
										class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
								</a></td>
								<td><a class="btn btn-warning" href="?edit=${item.id}">
										<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
								</a></td>
							</tr>
							<tr>
								<td><a class="btn btn-default"
									href="move?move=+1&id=${item.id}"> <span
										class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
								</a></td>
								<td><a class="btn btn-danger" href="delete?id=${item.id}">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</a></td>
							</tr>
						</table></td>

		</c:forEach>
	</tbody>
	</table>
</div>
</html>