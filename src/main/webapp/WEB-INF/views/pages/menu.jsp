<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<title>menu</title>

<html>
<div class="row">

	<div class="col-md-4">
		<ul class="nav nav-pills">
			<li role="presentation"><a
				href="<c:url value = '/menu/?available=true' />"> SHOW ONLY
					AVAILABLE/VISIBLE </a></li>
			<li role="presentation"><a
				href="<c:url value = '/menu/?available=false' />"> SHOW ALL </a></li>
		</ul>
	</div>

	<div class="col-md-4">
		<ul class="nav nav-pills">
			<c:forEach items="${categories}" var="menuCategory">
				<li role="presentation"><a href='#${menuCategory.name}'>${menuCategory.name}</a></li>
			</c:forEach>
		</ul>
	</div>

	<%-- <div class="col-md-4">

		<p>CurrentCustomer : ${currentCustomerId} </p>

		<c:if test="${customers!=null}">
		<span> wybierz uzytkownika </span>
			<f:form method="POST" commandName="customer" action="setCustomer">
				<f:select path="id" items="${customers}" itemLabel="email"
					itemValue="id" />
				<input type="submit" value="submit">
			</f:form>
		</c:if>
		
		<c:if test="${customers!=null}">
		<span> wybierz stolik </span>
			<f:form method="POST" commandName="table" action="setTable">
				<f:select path="id" items="${tables}" itemLabel="name"
					itemValue="id" />
				<input type="submit" value="submit">
			</f:form>
		</c:if>
	</div>

	<div class="col-md-4">
		<c:if test="${collCustOrder!=null}"> collCustOrder NOT EMPTY ${collCustOrder}</c:if>
	</div> --%>

</div>

<div class="row">

	<c:forEach items="${menu}" var="hashmap">
		<div class="row" id='${hashmap.key.name}'>

			<h1 class="bg-info" >
				<small> [#:${hashmap.key.sortNumber}]</small> ${hashmap.key.name} <small>
					[db.id:${hashmap.key.id}] </small>
			</h1>

		</div>
		<div class="row">

			<table
				class="table-striped table-hover table-condensed table-bordered">
				<tbody>
					<c:forEach items="${hashmap.value}" var="value">
						<tr>
							<td>${value.name}</td>

							<td>${value.description}</td>

							<td><br> ${value.price} PLN</td>

							<td>#:${value.sortNumber} id:${value.id}</td>
							<td><a href="order?id=${value.id}">
									<button class="btn btn-success">
										<s:message code="action.order" />
									</button>
							</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:forEach>
</div>

</html>