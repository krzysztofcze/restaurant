<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<title>category</title>

<c:if test="${category!=null}">
	<div class="row">
		<hr>
		<f:form class="form-inline" action="${post_url}" method="POST"
			modelAttribute="category">
			<div class="form-group">
				<f:hidden path="id" class="form-control" />
			</div>
			<div class="form-group">
				<label>Name</label>
				<f:input path="name" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>Numer porządkowy</label>
				<f:input type="number" class="form-control" path="sortNumber" />
			</div>
			<div class="form-inline">
				<label>Description</label>
				<f:textarea path="description" class="form-control" cols="69"
					rows="3"></f:textarea>
			</div>
			<div class="form-inline">
				<label>Visible</label>
				<f:checkbox path="visible" class="form-control" />
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">
					Submit <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</f:form>
	</div>
</c:if>
<hr>

<div class="row">

	<a class="btn btn-success" href="?edit=0"> Create new category <span
		class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	</a>

</div>

<hr>
<div class="row">
	<table class="table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th><s:message code="category.name" /></th>
				<th><s:message code="category.description" /></th>
				<th><s:message code="category.visible" /></th>
				<th>DB.ID</th>
				<th><s:message code="action" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${categories}" var="item">
				<tr>
					<td>${item.sortNumber}</td>
					<td>${item.name}</td>
					<td>${item.description}</td>
					<td>${item.visible}</td>
					<td>${item.id}</td>
					<td>
						<table>
							<tr>
								<td><a class="btn btn-default"
									href="move?move=-1&id=${item.id}"> <span
										class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
								</a></td>
								<td><a class="btn btn-warning" href="?edit=${item.id}">
										<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
								</a></td>
							</tr>
							<tr>
								<td><a class="btn btn-default"
									href="move?move=+1&id=${item.id}"> <span
										class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
								</a></td>
								<td><a class="btn btn-danger" href="delete?id=${item.id}">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</a></td>
							</tr>
						</table>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<hr>
</div>

