<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<title>customer</title>

<html>

<div class="row">

	<c:if test="${customer!=null}">
		<hr>
	
		<f:form class="form-inline" action="${post_url}" method="POST"
			modelAttribute="customer">
			<div class="form-group">
				<label></label>
				<f:hidden path="id" class="form-control" />
			</div>
			<div class="form-group">
				<label>Name</label>
				<f:input path="name" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>Surname</label>
				<f:input path="surname" type="text" class="form-control" />
			</div>

			<div class="form-group">
				<label>Email ( LOGIN )</label>
				<f:input path="email" type="text" class="form-control" required="true"/>
				<f:errors path="email" cssClass="btn btn-danger" element="button"></f:errors>
			</div>
			<div class="form-group">
				<label>Password</label>
				<f:password path="password" class="form-control" minlength="6" required="true"/>
				<f:errors path="password" class="btn btn-danger" element="button"></f:errors>
			</div>
			<div class="form-group">
				<label>Phone #</label>
				<f:input path="phoneNumber" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>Description</label>
				<f:textarea path="description" class="form-control" cols="69"
					rows="3"></f:textarea>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">
					Submit <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</f:form>
	</c:if>
</div>

<hr>

<div class="row">

	<a class="btn btn-success" href="<c:url value = '/customer/?add=1' />">
		Create new customer <span class="glyphicon glyphicon-plus"
		aria-hidden="true"></span>
	</a>

</div>

<hr>
<div class="row">
	<table class="table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Surname</th>
				<th>Email(Login)</th>
				<th>PASSWORD</th>
				<th>Phone #</th>
				<th>Description</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${customers}">
				<tr>
					<td>${item.name}</td>
					<td>${item.surname}</td>
					<td>${item.email}</td>
					<td> ****** </td>
					<td>${item.phoneNumber}</td>
					<td>${item.description}</td>

					<td><a class="btn btn-danger"
						href="<c:url value = '/customer/delete?id=${item.id}' />"> <span
							class="glyphicon glyphicon-remove" aria-hidden="true"></span></a> <a
						class="btn btn-warning"
						href="<c:url value = '/customer/?edit=${item.id}' />"> <span
							class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					</a></td>
			</c:forEach>
		</tbody>
	</table>
</div>

</html>