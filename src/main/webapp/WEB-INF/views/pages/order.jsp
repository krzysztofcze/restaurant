<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<title>orders</title>

<html>
<div class="row">
	<div class="row">
		<ul class="nav nav-pills">
			<li role="presentation"><a
				href="<c:url value = '/orders/kitchen' /> "> Kitchen </a></li>
			<li role="presentation"><a
				href="<c:url value = '/orders/waitress' /> "> Waitress </a></li>
		</ul>
	</div>
	<c:forEach items="${customerOrders}" var="hashmap">
		<div class="row">
			<h1 class="bg-info">
				[${hashmap.id}] ${hashmap.customer.email}
				[status:${hashmap.status.name}] <a class="btn btn-default"
					href="<c:url value = '/orders/next?id=${hashmap.id}' />"> <span
					class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
				</a> <a class="btn btn-danger"
					href="<c:url value = '/orders/cancel?id=${hashmap.id}' />"> <span
					class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</a>
			</h1>
			<table
				class="table-striped table-hover table-condensed table-bordered">
				<thead>
					<tr>
						<td>Product</td>
						<td>Quantity</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${hashmap.orderProduct}" var="items">

						<tr>
							<td>${items.product.name}</td>
							<td>${items.quantity}</td>
						</tr>

					</c:forEach>
				</tbody>
			</table>

		</div>
	</c:forEach>

</div>
</html>