<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<title>customer</title>

<html>

<hr>
<c:if test="${msgAfterPost!=null}">
	<div class="row">
		<p class="bg-info">
			${msgAfterPost}
			<f:errors path="*" />
		</p>
	</div>
</c:if>
<c:if test="${currentCustomer.id!=null}">
	<div class="row">
		<div class="row">
			<p>You are logged ass : ${currentCustomer.email}</p>
			<p>
				<a href="?logout=true">[ Logout ]</a>
			</p>
		</div>
	</div>
</c:if>


<c:if test="${currentCustomer.id==null}">
	<div class="row">

		<div class="col-lg-8">
			<h4>sign in</h4>

			<div class="row">

				<f:form id="login" class="form-inline" method="POST">
					<%-- <input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" /> --%>
					<div class="form-group">
						<label>email ( LOGIN ) </label> <input name="email" type="text"
							class="form-control" required />
						<f:errors path="email"></f:errors>
					</div>
					<div class="form-group">
						<label>password </label> <input name="password" type="password"
							class="form-control" required />
						<f:errors path="password"></f:errors>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success" name="formName"
							value="login" form="login">
							LOGIN <span class="glyphicon glyphicon-ok-circle"
								aria-hidden="true"></span>
						</button>
					</div>
				</f:form>
			</div>


			<hr>
			<h4>register</h4>
			<div class="row">

				<f:form id="register" class="form-inline" method="POST"
					modelAttribute="client">

					<c:set var="nameHasBindError">
						<f:errors path="name" />
					</c:set>

					<div
						class="form-group ${not empty nameHasBindError?'has-error':''}">
						<label class="control-label"
							for="${not empty nameHasBindError?'inputError1':''}">Name<span
							id="helpBlock" class="help-block">${nameHasBindError}</span></label>
						<f:input path="name" type="text" class="form-control"
							aria-describedby="helpBlock" required="true" />
					</div>

					<div class="form-group">
						<label>Surname</label>
						<f:input path="surname" type="text" class="form-control" />
					</div>

					<c:set var="emailHasBindError">
						<f:errors path="email" />
					</c:set>

					<div
						class="form-group ${not empty emailHasBindError?'has-error':''}">
						<label class="control-label"
							for="${not empty emailHasBindError?'inputError1':''}">Email(Login)<span
							id="helpBlock" class="help-block">${emailHasBindError}</span></label>
						<f:input path="email" type="text" class="form-control"
							aria-describedby="helpBlock" required="true" />
					</div>

					<div class="form-group">
						<label>Password</label>
						<f:password path="password" class="form-control" />
						<%-- required="true" --%>
						<%-- minlength="6" --%>
						<f:errors path="password" cssClass="btn btn-danger"
							element="button" />
					</div>
					<div class="form-group">
						<label>Phone #</label>
						<f:input path="phoneNumber" type="text" class="form-control" />
						<f:errors path="phoneNumber" />
					</div>
					<div class="form-group">
						<label>Description</label>
						<f:textarea path="description" class="form-control" cols="50"
							rows="3"></f:textarea>
						<f:errors path="description" />
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success" name="formName"
							value="register" form="register">
							REGISTER <span class="glyphicon glyphicon-ok-circle"
								aria-hidden="true"></span>
						</button>
					</div>
				</f:form>
			</div>

		</div>

		<div class="col-lg-4">
			<h4>order anonymous</h4>

			<div class="form-group">
				<f:form id="loginAsAnonymous" method="POST" >
					<button type="submit" class="btn btn-success" name="formName"
						value="loginAsAnonymous" form="loginAsAnonymous">
						Order anonymous <br> without creating account. <br> We create anonymous for this session.<span class="glyphicon glyphicon-ok-circle"
							aria-hidden="true"></span>
					</button>
				</f:form>
			</div>


		</div>
	</div>
</c:if>

</html>