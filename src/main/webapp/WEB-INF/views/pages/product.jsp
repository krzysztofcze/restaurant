<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<title>product</title>

<html>

<c:if test="${product!=null}">
	<div class="row">
		<hr>
		<f:form class="form-inline" action="${post_url}" method="POST"
			modelAttribute="product">
			<div class="form-group">
				<f:hidden path="id" class="form-control" />
			</div>
			<div class="form-group">
				<label>Numer porządkowy</label>
				<f:input type="number" class="form-control" path="sortNumber" />
			</div>
			<div class="form-group">
				<label>Kategoria</label>
				<f:select path="category.id" 
				itemValue="id"
				itemLabel="name"
				items="${categories}" class="form-control"></f:select>
			</div>
			<div class="form-group">
				<label>Name</label>
				<f:input path="name" type="text" class="form-control" />
			</div>
			<div class="form-inline">
				<label>Description</label>
				<f:textarea path="description" class="form-control" cols="69"
					rows="3"></f:textarea>
			</div>
			<div class="form-group">
				<label>Cena</label>
				<f:input type="number" class="form-control" path="price" />
			</div>
			<div class="form-group">
				<label>Widoczny</label>
				<f:checkbox path="available" class="form-control" />
			</div>
			<div class="form-group">
				<label>Allergeny</label>
				<f:input path="allergent" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>photo Url</label>
				<f:input path="photoUrl" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<label>video Url</label>
				<f:input path="videoUrl" type="text" class="form-control" />
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">
					Submit <span class="glyphicon glyphicon-ok-circle"
						aria-hidden="true"></span>
				</button>
			</div>
		</f:form>
	</div>
</c:if>
<hr>

<div class="row">

	<a class="btn btn-success" href="?edit=0"> Add new product <span
		class="glyphicon glyphicon-plus" aria-hidden="true"></span>
	</a>

</div>

<hr>
<div class="row">
	<table class="table-striped table-hover table-condensed table-bordered">
		<thead>
			<tr>
				<th><s:message code="product.id" /></th>
				<th><s:message code="product.name" /></th>
				<th><s:message code="product.description" /></th>
				<th><s:message code="product.price" /></th>
				<%-- <td><s:message code="product.available" /></td>
					<td><s:message code="product.sortNumber" /></td>
					<td><s:message code="product.allergent" /></td>
					<td><s:message code="product.photoUrl" /></td>
					<td><s:message code="product.videoUrl" /></td> --%>
				<th><s:message code="action" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${products}" var="item">
				<c:if test="${item.available==true}">
					<tr>
						<td>${item.id}</td>
						<td>${item.name}</td>
						<td>${item.description}</td>
						<td>${item.price}</td>
						<%-- <td>${item.available}</td>
							<td>${item.sortNumber}</td>
							<td>${item.allergent}</td>
							<td>${item.photoUrl}</td>
							<td>${item.videoUrl}</td> --%>
						<td>
							<table>
								<tr>
									<td><a class="btn btn-default"
										href="move?move=-1&id=${item.id}"> <span
											class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
									</a></td>
									<td><a class="btn btn-warning" href="?edit=${item.id}">
											<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
									</a></td>
								</tr>
								<tr>
									<td><a class="btn btn-default"
										href="move?move=+1&id=${item.id}"> <span
											class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
									</a></td>
									<td><a class="btn btn-danger" href="delete?id=${item.id}">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</a></td>
								</tr>
							</table>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
</div>