<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>
<title>customer order</title>

<html>

<div class="row">
	<hr>
	<c:if test="${customerTable.id!=null}">
		<h3>Current table :</h3>
		<c:out value='${sessionScope.customerTable.name}' />
		<a href="<c:url value = '/' />"> [ change table ] </a>
	</c:if>

	<c:if test="${customerTable.id==null}">
		<h3>
			<a href="<c:url value = '/' />">Please [ choose a table ]</a> .
		</h3>
		<c:out value='${sessionScope.customerTable.name}' />
	</c:if>
</div>

<div class="row">
	<hr>
	<c:if test="${currentCustomer.email!=null}">
		<h3>Current customer :</h3>
		<c:out value='${currentCustomer.email}'>
		</c:out>
		<a href="<c:url value = '/logout' />"> [ LogOut ]</a>
	</c:if>
	<c:if test="${currentCustomer.email==null}">
		<h3>
			Please <a href="<c:url value = '/login' />">[ login ]</a> , <a
				href="<c:url value = '/register' />"> [ Register ]</a> or <a
				href="<c:url value = '/anonymouse' />">[ order anonymous ]</a>
		</h3>
	</c:if>
</div>

<c:if test="${currentCustomerOrder!=null}">
	<hr>
	<h3>current customer order</h3>
	<div class="row">
		<h3></h3>
		<table
			class="table-striped table-hover table-condensed table-bordered">
			<thead>
				<tr>
					<th colspan="2">Customer Order Id : ${currentCustomerOrder.id}</th>
					<th colspan="3">Status : "${currentCustomerOrder.status.name}" [
						id : ${currentCustomerOrder.status.id} ]</th>
				</tr>
				<tr>
					<th>product name</th>
					<th>price</th>
					<th>quantity</th>
					<th>value</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${currentCustomerOrder.orderProduct}" var="items">
					<tr>
						<td>${items.product.name}</td>
						<td>${items.product.price}</td>
						<td>
							<table>
								<tr>
									<td rowspan="2">${items.quantity} </td>
									<td> <a class="btn btn-default"
										href="<c:url value = '/customerorder/quantity?id=${items.product.id}&change=+1' />">
											<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
									</a></td>
								</tr>
								<tr>
									<td><a class="btn btn-default"
										href="<c:url value = '/customerorder/quantity?id=${items.product.id}&change=-1' />">
											<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
									</a></td>
								</tr>
							</table>
						</td>

						<td>${items.value}</td>
						<td><a class="btn btn-danger"
							href="<c:url value = '/customerorder/delete?id=${items.product.id}' />">
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"></td>
					<td>Total =</td>
					<td><c:out value='${currentCustomerOrder.total}' /> PLN</td>
					<td></td>
				</tr>
			</tfoot>
		</table>

		<f:form method="POST">
			<button class="btn btn-success" type="submit" name="accept"
				value="submit">CONFIRM ORDER</button>

			<button class="btn btn-default" type="submit" name="cancel"
				value="submit">CANCEL ORDER</button>
		</f:form>

	</div>
</c:if>


</html>