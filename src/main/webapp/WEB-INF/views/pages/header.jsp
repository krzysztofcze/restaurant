<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<div class="row">

	<img class="header"
		src="${pageContext.request.contextPath}/resources/coffee-header_.png"
		height="120px" />

</div>
<div class="row">
	<div class="col-lg-9">

		<ul class="nav nav-pills">
			<li role="presentation"><a href="<c:url value = '/' />"> / </a></li>
			<li role="presentation"><a href="<c:url value = '/menu/' />">
					MENU </a></li>
			<li role="presentation"><a href="<c:url value = '/products/' />">
					<s:message code="products" />
			</a></li>
			<li role="presentation"><a
				href="<c:url value = '/categories/' />"> <s:message
						code="category" />
			</a></li>

			<li role="presentation"><a href="<c:url value = '/statuses/' />">
					status </a></li>
			<li role="presentation"><a href="<c:url value = '/tables/' />">
					tables </a></li>
			<li role="presentation"><a
				href="<c:url value = '/customers/' />"> customers </a></li>
			<li role="presentation"><a
				href="<c:url value = '/customersorder/' />"> current customers
					order </a></li>
			<li role="presentation"><a href="<c:url value = '/orders/' />">
					order </a></li>
			<li role="presentation"><a
				href="<c:url value = '/cookiesandsession/' />"> ciastka i sesje
					<br> {for me 2 del}
			</a></li>
		</ul>

	</div>
	<div class="col-lg-3">
		<div class="row">
			<c:out value='${sessionScope.customerTable.name}' />
		</div>

		<div class="row">
			<c:if test="${currentCustomer.email!=null}">
				<c:out value='${currentCustomer.email}'>
				</c:out>
				<a href="<c:url value = '/customer_logout' />"> [ LogOut ]</a>
			</c:if>
			<c:if test="${currentCustomer.email==null}">
				<a href="<c:url value = '/customer_login' />"> [ LogIn ]</a>
				<br>
				<a href="<c:url value = '/customer_register' />"> [ Register ]</a>
			</c:if>
		</div>
		
		<c:if test="${pageContext.request.isUserInRole('ADMIN')||pageContext.request.isUserInRole('USER')}">
		<div class="row">
			<p>Admin: ${pageContext.request.isUserInRole('ADMIN')}</p>
			<p>User: ${pageContext.request.isUserInRole('USER')}</p>
			<a href="<c:url value = '/login' />"><button>LOGIN</button></a>
			<form action="<c:url value = '/logout' />" method="POST">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="submit" value="LOGOUT">
			</form>
		</div>
		</c:if>
		<c:if test="${currentCustomerOrder!=null}">
			<div class="row">
				<c:if test="${currentCustomerOrder.status.id>0}">
					<h4>Order status : ${currentCustomerOrder.status.name}</h4>
				</c:if>
				<ul>
					<c:forEach items="${currentCustomerOrder.orderProduct}" var="items">
						<li>${items.product.name}:${items.product.price}PLNx
							${items.quantity} = ${items.value}</li>
					</c:forEach>
				</ul>
				<h4>
					Total :
					<c:out value='${currentCustomerOrder.total}' />
					PLN
				</h4>
				<form method="POST"
					action="${pageContext.request.contextPath}/customerorder/">

					<a href="<c:url value = '/customerorder/' />"><button
							class="btn btn-default">EDIT order</button></a>

					<button class="btn btn-success" type="submit" name="accept"
						value="submit">CONFIRM & ORDER</button>

				</form>

			</div>
		</c:if>
	</div>
</div>

<hr>