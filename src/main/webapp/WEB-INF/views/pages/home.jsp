<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<title>welcome</title>

<html>


<div class="row">
	<div>chose/change a table</div>

	<%-- 	
	<c:if test=" ">
		<div>
			<span> wybierz stolik </span>
			<f:form method="POST" commandName="table" action="setTable">
				<f:select path="id" items="${tables}" itemLabel="name"
					itemValue="id" />
				<input type="submit" value="submit">
			</f:form>
		</div>
		<div>
			<f:form method="POST" commandName="table">
				<f:select path="id" items="${tables}" itemLabel="name"
					itemValue="id" class="form-control" />
				<input type="submit" value="submit">
			</f:form>
		</div>
	</c:if> 
--%>

	<div class="row">
		<form class="form-inline">
			<c:forEach var="item" items="${tables}">
				<%-- <f:form class="form-group "  action="${post_url_register}"
					method="POST" modelAttribute="currentTable"> --%>

					<button class="btn btn-success" name="setTable" value="${item.id}"
						type="submit" formmethod="POST">

						${item.name} ( ${item.description} ) <span
							class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
					</button>
				<%-- </f:form> --%>
			</c:forEach>
		</form>
	</div>


	<div>register | sign in | order anonymous</div>
	<div class="row">

		<div class="col-lg-4">
			<h4>register</h4>
			<hr>
			<div class="row">

				<c:url var="post_url_register" value="/customer_register" />
				<f:form class="form-inline" action="${post_url_register}"
					method="POST" modelAttribute="Customer" object="Customer">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<div class="form-group">
						<label>Name</label>
						<f:input path="name" type="text" class="form-control" />
						<f:errors path="name" />
					</div>
					<div class="form-group">
						<label>Surname</label>
						<f:input path="surname" type="text" class="form-control" />
						<f:errors path="surname"/>
					</div>

					<div class="form-group">
						<label>Email ( LOGIN )</label>
						<f:input path="email" type="text" class="form-control"
							required="true" />
						<f:errors path="email"/>
					</div>
					<div class="form-group">
						<label>Password</label>
						<f:password path="password" class="form-control" minlength="6"
							required="true" />
						<f:errors path="password" cssClass="btn btn-danger" element="button"/>
					</div>
					<div class="form-group">
						<label>Phone #</label>
						<f:input path="phoneNumber" type="text" class="form-control" />
						<f:errors path="phoneNumber"/>
					</div>
					<div class="form-group">
						<label>Description</label>
						<f:textarea path="description" class="form-control" cols="50"
							rows="3"></f:textarea>
						<f:errors path="description"/>
					</div>
					<br>
					<div class="form-group">
						<button type="submit" class="btn btn-success">
							Submit <span class="glyphicon glyphicon-ok-circle"
								aria-hidden="true"></span>
						</button>
					</div>
				</f:form>

			</div>
		</div>
		<div class="col-lg-4">
			<h4>sign in</h4>
			<hr>

			<c:if test="${currentCustomer.id==null}">
				<div class="row">
					<c:url var="post_url_login" value="/customer_login" />
					<f:form class="form-inline" action="${post_url_login}"
						method="POST">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="form-group">
							<label>email ( LOGIN ) </label> <input name="email" type="text"
								class="form-control" required />
							<f:errors path="email"/>
						</div>
						<div class="form-group">
							<label>password </label> <input name="password" type="password"
								class="form-control" required />
							<f:errors path="password"/>
						</div>
						<br>
						<div class="form-group">
							<button type="submit" class="btn btn-success">
								LOGIN <span class="glyphicon glyphicon-ok-circle"
									aria-hidden="true"></span>
							</button>
						</div>
					</f:form>
				</div>
			</c:if>


		</div>
		<div class="col-lg-4">
			<h4>order anonymous</h4>
			<hr>
			<div>
				<div class="form-group">
					<a href="anonymous" type="submit" class="btn btn-success">
						Order anonymous <br> without account <span
						class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>
					</a>
				</div>


			</div>


		</div>

	</div>

	<div>order from menu</div>


	<div>review order and confirm/cancel</div>


	<div>status of your order</div>
</div>


</html>