<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="decorator"
	uri="http://www.opensymphony.com/sitemesh/decorator"%>

<title>cookies session and security</title>

<html>

<div class="row">
	<h4>Security</h4>
	<ul>
		<li><p>
				URI: ${uri} <br /> User : ${user} <br /> roles: ${roles} <br />
				<br /></li>
	</ul>

</div>

<hr>

<div class="row">
	<h4>Cookies</h4>
</div>
<c:if test="${ciasteczka!=null}">
	<div class="row">
		<ul>
			<c:forEach items="${ciasteczka}" var="items">
				<li>${items}</li>
			</c:forEach>
		</ul>
	</div>
</c:if>

<c:if test="${listac!=null}">
	<div class="row">
		<ul>
			<li>${listac}</li>
		</ul>
	</div>
</c:if>

<hr>

<div class="row">
	<h4>Session</h4>
</div>

<c:if test="${sesja!=null}">
	<div class="row">
		<ul>
			<c:forEach items="${sesja}" var="itemss">
				<c:forEach items="${itemss}" var="itemsss">
					<c:if test="${itemsss=='currentCustomer'}">
						<li>${itemsss}</li>
					</c:if>

					<c:if test="${itemsss=='currentCustomerTableId'}">
						<li>${itemsss}</li>
					</c:if>
					<li>bez ifa: ${itemsss}</li>
				</c:forEach>
			</c:forEach>
		</ul>
	</div>
</c:if>

<c:if test="${listas!=null}">
	listas != null<br />
	${listas}<br />

</c:if>

<c:if test="${customer!=null}">
	<hr>
	customer klient != null<br />
	${Customer}<br />

</c:if>

<c:if test="${currentCustomer!=null}">
	<hr>
	currentCustomer klient != null<br />
	${currentCustomer}<br />

</c:if>

<c:if test="${table!=null}">
	<hr>
	table stolik != null<br />
	${table}<br />

</c:if>
<c:if test="${customerTable!=null}">
	<hr>
	customerTable stolik != null<br />
	${customerTable}<br />

</c:if>
<c:if test="${s!=null}">
	<hr>
	
	${s}<br />

</c:if>

</html>