%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menu</title>
</head>
<body>
	<div class="container">
		<div class="container header">
			<h3>header</h3>
			<%@ include file="/WEB-INF/views/header.jsp"%>
		</div>
		<div class="container body">

			<div class="row">

				<div class="col-md-4">
					<ul class="nav nav-pills">
						<li role="presentation"><a
							href="<c:url value = '/menu/?available=true' />"> SHOW ONLY
								AVAILABLE/VISIBLE </a></li>
						<li role="presentation"><a
							href="<c:url value = '/menu/?available=false' />"> SHOW ALL </a></li>
					</ul>
				</div>

				<div class="col-md-4">

					<p>CurrentCustomer : ${curretnCustomer}</p>

					<c:if test="${customers!=null}">
						<f:form method="POST" commandName="customer" action="setCustomer">
							<f:select path="id" items="${customers}" itemLabel="email"
								itemValue="id" />
							<input type="submit" value="submit">
						</f:form>
					</c:if>
				</div>
				<div class="col-md-4">
					<c:if test="${collCustOrder!=null}"> collCustOrder NOT EMPTY 
				
					${collCustOrder}
					
				</c:if>
				</div>
			</div>

			<div class="row">

				<c:forEach items="${menu}" var="hashmap">
					<div class="row">
						<h1 class="bg-info">${hashmap.key.name}
							<small> id:${hashmap.key.id} #:${hashmap.key.sortNumber}
							</small>
						</h1>

					</div>
					<div class="row">

						<table
							class="table-striped table-hover table-condensed table-bordered">
							<tbody>
								<c:forEach items="${hashmap.value}" var="value">
									<tr>
										<td>${value.name}</td>

										<td>${value.description}</td>

										<td><br> ${value.price} PLN</td>

										<td>#:${value.sortNumber} id:${value.id}</td>
										<td><a href="order?id=${value.id}">
												<button class="btn btn-success">
													<s:message code="action.order" />
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:forEach>

			</div>
		</div>

		<hr>

		<div class="container footer">
			<h3>footer</h3>
		</div>
	</div>
</body>
</html>